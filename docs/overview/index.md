```{toctree}
:maxdepth: 3


```
# Overview

Takkan is an application framework designed to speed up the development of a Flutter application, primarily where there is a significant connection to data.

It aims to remove many of the repetitive elements of connecting to and presenting data to the user, while leaving the developer completely free to use the full presentation power of Flutter.


## Concept

These are the primary components of Takkan:

- Schema.  Defines application data types, user access control and validation, while also supporting version control.
- CloudCodeGenerator.  Currently available only for Back4App, this generates server side Javascript from the `Schema` to provide server side validation and version controlled client access.
- Composer. Uses the Schema to bind presentation widgets to the relevant data.  It provides high level layouts, forms and client side validation.
- ClientCodeGenerator.  If necessary, a code generator may be developed to generate client side code rather than the default 'interpreted' method.  This is not currently a priority.




