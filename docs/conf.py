# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Takkan'
copyright = '2023, David Sowerby'
author = 'David Sowerby'
release = '0.0.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ["myst_parser", "sphinx_immaterial"]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

myst_enable_extensions = [
    "colon_fence",
]





# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_immaterial"
# material theme options (see theme.conf for more information)
html_theme_options = {
    "icon": {
        "repo": "fontawesome/brands/gitlab",
        "edit": "material/file-edit-outline",
    },
    "site_url": "https://takkan.org/",
    "repo_url": "https://gitlab.com/dsowerby/takkan/",
    "repo_name": "Takkan",
    "edit_uri": "blob/develop/docs",
    "globaltoc_collapse": True,
    "features": [
        "navigation.expand",
        # "navigation.tabs",
        # "toc.integrate",
        "navigation.sections",
        # "navigation.instant",
        # "header.autohide",
        "navigation.top",
        # "navigation.tracking",
        # "search.highlight",
        "search.share",
        "toc.follow",
        "toc.sticky",
        "content.tabs.link",
        "announce.dismiss",
    ],
    "palette": [
        {
            "media": "(prefers-color-scheme: light)",
            "scheme": "default",
            "accent": "deep-purple",
            "primary": "deep-purple",
            "toggle": {
                "icon": "material/lightbulb-outline",
                "name": "Switch to dark mode",
            },
        },
        {
            "media": "(prefers-color-scheme: dark)",
            "scheme": "slate",
            "primary": "blue",
            "accent": "lime",
            "toggle": {
                "icon": "material/lightbulb",
                "name": "Switch to light mode",
            },
        },
    ],
    # BEGIN: version_dropdown
    "version_dropdown": True,
#     "version_info": [
#         {
#             "version": "https://sphinx-immaterial.rtfd.io",
#             "title": "ReadTheDocs",
#             "aliases": [],
#         },
#         {
#             "version": "https://jbms.github.io/sphinx-immaterial",
#             "title": "Github Pages",
#             "aliases": [],
#         },
#     ],
    # END: version_dropdown
    "toc_title_is_page_title": True,
    # BEGIN: social icons
    "social": [
#         {
#             "icon": "fontawesome/brands/gitlab",
#             "link": "https://gitlab.com/dsowerby/takkan",
#             "name": "Source on gitlab.com",
#         },
#         {
#             "icon": "fontawesome/brands/flutter",
#             "link": "https://pypi.org/project/sphinx-immaterial/",
#         },
    ],
    # END: social icons
}

# end html_theme_options
html_static_path = ['_static']
html_title = f"{project} {release}"
