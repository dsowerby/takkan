# User Guide

## Primary Components

### Schema

At the heart of the Takkan framework is the `Schema`.  

```{include} /common/schema-features.md
```

Developers define an application schema, then use it to generate server side code using a [ClientCodeGenerator](#client-code-generator).

The principal client-side component, the [Composer](#composer), also uses the `Schema` to support client side validation and permissions handling.

The version controlled nature of the `Schema` allows application users to continue to operate on a deprecated version until such time as they update.


:::{note}
Server-side code generation is currently limited to Back4App, although in theory it could be extended to other backends.  To do so is not currently a project priority, but contributions are welcome.
:::

### Composer

A `Composer` is the primary client side component, and is mostly about presenting data to the user, or the user editing that data.

It uses the same `Schema` as is used to generate the server side code and schema. This avoids potential discrepancies between client and server side code, especially when clients may be operating on differing versions.

The `Composer` provides client-side validation, and allows the developer to, for example, hide or disable components that display data that the end user does not have permission to view.

It also provides default behaviour that suits many situations, over and above that provided by Flutter.  For example, read / edit / save / cancel functionality is available with minimal developer effort.

Currently, the Takkan app runs a `Composer` in an "interpreted" mode.  However, it is recognised that this may have some performance limitations for the more demanding apps.

## Client Code Generator

In the event that performance becomes an issue, it is intended that a `ClientCodeGenerator` would generate native Flutter code from the `Composer`.  This would remove any need for interpreting the `Composer` at run time, but would lose the flexibility of the being able to dynamically download a version update of the `Composer`.

This aspect of the project has not been started, and will only become a priority if performance does actually become an issue.


## Creating an Application