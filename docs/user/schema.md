# Schema

# SchemaSet

Although the `Schema` is key to Takkan's operation and is a standalone object, it is defined as part of a `SchemaSet`.  A `SchemaSet` comprises a full `Schema` definition as `baseVersion` plus 0..n `diffs`. These are `SchemaDiff` instances and define subsequent versions.

This means the developer only has to define changes at each version above the `baseVersion`.

You can then retrieve a full `Schema` definition via `SchemaSet.schemaVersion(versionIndex)` - the `SchemaSet` expands the diffs to provide a full definition.

### Version Control

Each `Schema` or `SchemaDiff` instance contains a [`Version`](version.md) object.




```dart
final Schema schema = Schema(
        name: 'test',
        version: const Version(number: 0),
        documents: {
          'Person': Document(
            fields: {
              'firstName': FString(),
              'lastName': FString(),
              'age': FInteger(),
            },
            queries: {
              'adults': Query(
                conditions: [
                  C('age').int.equalTo(152),
                  C('lastName').string.equalTo('Hazel'),
                ],
                queryScript: "firstName == 'Jack'",
                returnSingle: true,
              ),
            },
          ),
        },
      );
```