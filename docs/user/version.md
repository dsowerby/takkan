# Version

Within its context, that is, within a [`VersionSet`](#versionset), each `Version` must have a unique `versionIndex`, with higher numbers being later versions.

Diffs are then expanded (layered on top of the `baseVersion`) in ascending order of `versionIndex`.

## Version Status

A `Version` object also contains a `VersionStatus` for tracking which versions are released, deprecated etc.

Rules regarding transition between states are provided by an implementation of `VersionStatusChangeRules`. 

By default the only restriction in how the status is managed is that only one version can be in a released state.

You can provide your own rules with an implementation of `VersionStatusChangeRules` and pass it to the `VersionSet` constructor.


## VersionSet

A `VersionSet` is a common base class for the top most version controlled objects, namely:

- `SchemaSet`
- `ComposerSet`
