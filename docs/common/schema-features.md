The main features of the `Schema` and its associated classes are:

- Defines data types
- Defines field validation
- Defines user roles
- Defines user access rules
- Single reference for generated server-side code, and client code
- Version controlled, enabling support for concurrent released and deprecated application versions