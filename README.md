# Takkan

## README

Takkan is an Flutter application framework, designed to take away many of the repetitive elements of application design, while leaving the developer completely free to use the full power of Flutter.

See [documentation](https://docs.page/davidsowerby/takkan)


## Documentation

### Build and Serve

From project root:

```bash
sphinx-autobuild docs docs/_build/html
```

But you do need to click on the terminal window to trigger autobuild

Browse to http://localhost:8000/index.html


## Local Build

sudo apt-get install python3-sphinx
pip install sphinx-autobuild
pip install sphinx-immaterial
pip install myst-parser



