// ignore_for_file: must_be_immutable
/// See comments on [TakkanElement]
import 'package:change_case/change_case.dart';
import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:takkan_common/common/constants.dart';
import 'package:takkan_common/common/inherited.dart';
import 'package:takkan_common/common/log.dart';
import 'package:takkan_common/common/walker.dart';

import '../../data/json_converter.dart';
import '../../data/select/condition.dart';
import '../../data/select/condition_final.dart';
import '../../data/select/expression.dart';
import '../../data/select/operation.dart';
import '../common/schema_element.dart';
import '../document/document.dart';
import 'index.dart';

part 'field.g.dart';

/// [MODEL] the data type of the model attribute represented
///
/// - [index] is the type of index to apply to this field, if any.  See [Index]
///
/// Validation can be defined by either or both of the following properties:
/// - [validation] is a script version of validation, for example '> 100'
/// - [conditions] defines validation but in a code format.  The easiest way
/// to define these are with the helper class 'V', for example:  V.int.greaterThan(0)
///
/// Both achieve the same thing, and if both are defined, they are combined,
/// during a call to [asFinal].  The [conditions] property then contains both
/// the original conditions and the [Condition] instances derived from [validation]
///
/// [document] is the document to which this field belongs, and only needs to be
/// declared here if it is not already declared in the parent chain.  In effect,
/// this field will be attached to the document declared here, or the nearest
/// point when walking back up the tree.
@JsonSerializable(explicitToJson: true)
class Field<MODEL>
    with Walkable
    implements HasFinal<FieldFinal<dynamic>>, SchemaElement, LateNamed {
  Field({
    bool? required,
    this.defaultValue,
    this.validation,
    bool? readOnly,
    Index? index,
    String? property,
    List<Condition<MODEL, dynamic>> conditions = const [],
  })  : index = index ?? Index.none,
        _property = property,
        required = required ?? false,
        _conditions = conditions,
        _readOnly = readOnly;

  factory Field.fromJson(Map<String, dynamic> json) => _$FieldFromJson(json);

  @JsonKey(includeToJson: false, includeFromJson: false)
  @override
  String get property => checkInherited(this, _property);
  String? _property;
  bool? _readOnly;

  bool get readOnly => checkInherited(this, _readOnly);

  // String get name => inherited.property;
  @override
  void lateName(String name) => _property = name;

  final List<Condition<MODEL, dynamic>> _conditions;

  /// We are already persisting [_conditions], which seems to need to be of
  /// type List<Condition<dynamic>> for the Json conversion to work.  Hence the
  /// need to cast here.
  @JsonKey(includeToJson: false, includeFromJson: false)
  List<Condition<MODEL, dynamic>> get conditions => List.castFrom(_conditions);

  Document get document =>
      throw UnimplementedError('Field does not have a document');

  List<Object?> get props => [
        conditions,
        required,
        index,
        defaultValue,
        document,
        validation,
      ];

  final Index index;

  final String? validation;
  final bool required;
  @DataTypeConverter()
  final MODEL? defaultValue;

  String get cloudTypeLabel {
    switch (modelType) {
      case int:
        return 'integer';
      default:
        throw UnsupportedError('Unsupported model type: $modelType');
    }
  }

  /// The [validation] expanded to include property name, for example: '> 100 && <200'
  /// becomes 'property > 100 && property < 200'
  String get validationScript {
    final v = validation;
    if (v == null || v.isEmpty) {
      return '';
    }

    final statements = v.split(statementSeparator);
    final expandedStatements = <String>[];
    for (final String statement in statements) {
      final List<String> clauses = statement.split(' ');
      clauses.removeWhere((element) => element.trim().isEmpty);
      String part0 = '';
      for (var i = 0; i < clauses.length; i++) {
        final index = i % 2;
        if (index == 0) {
          part0 = clauses[i];
        } else {
          expandedStatements.add('$property $part0 ${clauses[i]}');
        }
      }
    }
    return expandedStatements.join(' $statementSeparator ');
  }

  String get cloudImportName => 'validate${cloudTypeLabel.toCapitalCase()}';

  String get cloudImportStatement =>
      "const $cloudImportName = require('./${cloudTypeLabel}_validation.js');";

  /// True only for [FPointer] and [FRelation] TODO: This currently always returns false
  bool get isLinkField => false;

  bool get hasValidation => required || (conditions.isNotEmpty);

  Type get modelType => MODEL;

  @override
  Map<String, dynamic> toJson() => _$FieldToJson(this);
  @override
  List<Object?> get subElements => [];

  @override
  FieldFinal<MODEL> get asFinal {
    return FieldFinal<MODEL>(
        required: required,
        readOnly: readOnly,
        conditions: IListConst(conditions.map((e) => e.asFinal).toList()));
  }
}

class FieldFinal<MODEL> implements Final {
  const FieldFinal({
    required this.conditions,
    required this.required,
    required this.readOnly,
  });

  final IListConst<ConditionFinal<MODEL>> conditions;
  final bool required;
  final bool readOnly;

  /// Returns a list of [ValidationFailure], one for each failed condition, or
  /// an empty list if there are no failures
  List<ValidationFailure<MODEL>> validate(MODEL value) {
    if (conditions.isEmpty) {
      return List.empty();
    }
    final failures = <ValidationFailure<MODEL>>[];
    for (final ConditionFinal<MODEL> condition in conditions) {
      if (!condition.evaluate(value)) {
        failures.add(ValidationFailure(
          operator: condition.operator,
          operand: condition.operand,
          value: value,
        ));
      }
    }
    return failures;
  }

  bool get hasValidation => required || (conditions.isNotEmpty);
}

/// Although most implementations are covered by [Field], there will be a
/// requirement for some shortcut common [Field] implementations, for example
/// to represent the system clock.
class FieldJsonConverter
    extends JsonConverter<Field<dynamic>, Map<String, dynamic>> {
  const FieldJsonConverter();

  @override
  Field<dynamic> fromJson(Map<String, dynamic> json) {
    final elementType = json[jsonClassKey];
    switch (elementType) {
      case 'Field':
        return Field.fromJson(json);

      default:
        final msg = 'Field type $elementType not recognised';
        logType(runtimeType).e(msg);
        throw Exception(msg);
    }
  }

  @override
  Map<String, dynamic> toJson(Field<dynamic> object) {
    final type = object.runtimeType;
    final outputMap = <String, dynamic>{};
    outputMap[jsonClassKey] = type.toString();
    outputMap.addAll(object.toJson());
    return outputMap;
  }
}

class ValidationFailure<MODEL> {
  const ValidationFailure({
    required this.operator,
    required this.value,
    required this.operand,
  });

  final Operator operator;
  final MODEL value;
  final dynamic operand;
}
