// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'static_field.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StaticField<MODEL> _$StaticFieldFromJson<MODEL>(Map<String, dynamic> json) =>
    StaticField<MODEL>(
      text: json['text'] as String,
    );

Map<String, dynamic> _$StaticFieldToJson<MODEL>(StaticField<MODEL> instance) =>
    <String, dynamic>{
      'text': instance.text,
    };
