import 'package:json_annotation/json_annotation.dart';

import '../../data/json_converter.dart';
import '../../data/select/condition.dart';
import '../common/diff.dart';
import 'field.dart';
import 'index.dart';

part 'field_diff.g.dart';

/// Captures changes to a field as part of a [DocumentDiff].  Properties are
/// nullable as the intent is to require specification of only those fields that
/// are changed
///
/// The apparently unnecessary set up of [constraints], and passing them to [_constraints]
/// is to facilitate the use of a common Condition converter
///
/// Note the use of [removeDefaultValue].  Set this to true if you wish to clear
/// the base default value to null.  Setting [defaultValue] to null does not work
/// for this purpose, as a null diff represents 'no change'
///
@JsonSerializable(explicitToJson: true)
class FieldDiff<MODEL> implements Diff<Field<MODEL>> {
  FieldDiff({
    this.required,
    this.defaultValue,
    this.validation,
    this.index,
    this.property,
    this.removeDefaultValue = false,
    List<Condition<MODEL,dynamic>>? constraints,
  }) : _constraints = constraints;

  factory FieldDiff.fromJson(Map<String, dynamic> json) =>
      _$FieldDiffFromJson(json);
  bool removeDefaultValue;

  final Index? index;
  final List<Condition<dynamic,dynamic>>? _constraints;

  @JsonKey(includeToJson: false, includeFromJson: false)
  List<Condition<MODEL,dynamic>>? get constraints =>
      _constraints as List<Condition<MODEL,dynamic>>?;

  final String? validation;
  final bool? required;
  final String? property;
  @DataTypeConverter()
  final MODEL? defaultValue;

  /// Applies this diff to the [base]
  @override
  Field<MODEL> applyTo(Field<MODEL> base) {

    return Field<MODEL>(
      property: property,
      defaultValue:
          removeDefaultValue ? null : defaultValue ?? base.defaultValue,
      required: required ?? base.required,
      index: index ?? base.index,
      validation: validation ?? base.validation,
      conditions: constraints ?? base.conditions,
    );
  }

  Map<String, dynamic> toJson() => _$FieldDiffToJson(this);
}


