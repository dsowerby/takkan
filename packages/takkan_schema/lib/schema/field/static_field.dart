import 'package:json_annotation/json_annotation.dart';

import '../document/document.dart';
import 'field.dart';

part 'static_field.g.dart';

/// Static data
@JsonSerializable(explicitToJson: true)
class StaticField<MODEL> extends Field<MODEL> {
  StaticField({required this.text});

  factory StaticField.fromJson(Map<String, dynamic> json) =>
      _$StaticFieldFromJson(json);

  final String text;

  @override
  Map<String, dynamic> toJson() => _$StaticFieldToJson(this);

  @override
  Document get document => throw UnimplementedError();

  @override
  bool get hasValidation => false;

  @override
  // TODO: implement property
  String get property => throw UnimplementedError();
}
