import 'package:takkan_common/common/data.dart';
import 'package:takkan_common/common/version.dart';

import '../schema.dart';
/// [appVersion] is the version of the application, and is held here as well,
/// because this schema is used separately from the Composer which 'owns' the
/// version
class AppSchema {
  const AppSchema({
    required this.instanceSchemas,
    required this.appVersion,
  });

  final Map<DatasourceInstanceFinal, Schema> instanceSchemas;
  final Version appVersion;
}
