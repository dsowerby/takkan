// ignore_for_file: must_be_immutable
import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:takkan_common/common/data.dart';
import 'package:takkan_common/common/inherited.dart';
import 'package:takkan_common/common/version.dart';

import 'app/app_schema.dart';
import 'document/document.dart';

// part 'schema.g.dart';



// @JsonSerializable(explicitToJson: true)
// class SchemaDiff with DiffUpdate implements Diff<Schema> {
//   SchemaDiff({
//     this.addDocuments = const {},
//     this.removeDocuments = const [],
//     this.amendDocuments = const {},
//     this.isReadOnly,
//   });
//
//   factory SchemaDiff.fromJson(Map<String, dynamic> json) =>
//       _$SchemaDiffFromJson(json);
//   final Map<String, Document> addDocuments;
//   final List<String> removeDocuments;
//   final Map<String, DocumentDiff> amendDocuments;
//   final bool? isReadOnly;
//
//
//   Map<String, dynamic> toJson() => _$SchemaDiffToJson(this);
//
//   /// Returns a new [Schema] instance with this [SchemaDiff] applied.
//   ///
//   /// Changes are applied in this order:
//   ///  - amend
//   ///  - remove
//   ///  - add
//   ///
//   /// This means that it is possible, with a poorly defined [diff], to remove a
//   /// document and add it back in, or to amend a document and then remove it,
//   /// all within one [diff].
//   @override
//   Schema applyTo(Schema base) {
//     return Schema(
//       documents: _updatedDocuments(base),
//     );
//   }
//
//   Map<String, Document> _updatedDocuments(Schema base) {
//     return updateSubElements(
//       baseSubElements: base.documents,
//       amendSubElements: amendDocuments,
//       removeSubElements: removeDocuments,
//       addSubElements: addDocuments,
//     );
//   }
//
//   VersionStatus get status=>throw UnimplementedError('get status');
//   int get versionIndex => throw UnimplementedError('get versionIndex');
//   Version get version => throw UnimplementedError('get version');
//   set version(Version version) => throw UnimplementedError('set version');
// }





class SchemaException implements Exception {
  const SchemaException(this.msg);

  final String msg;

  String errMsg() => msg;
}



/// [Schema] is a definition of a data structure, including data types, validation,
/// permissions and relationships.  It provides a common definition for use by
/// the client and server side code.
///
/// It is not created directly by the developer, but collated from the Documents
/// and Fields declared within the Composer.
///
/// The terminology used reflects the intention to keep this backend-agnostic,
/// although there may be cases where a backend does not support a particular
/// data type.
///
/// How these translate to the structure in the backend will depend on the
/// backend itself, and the user's preferences.
///
///
/// - In [documents], the map key is the document name, and is repeated as
/// [Document.className] in the document itself.  This is, for example, a
/// Back4App Class or a Firestore collection, as determined by the backend
/// implementation.
///
/// The schema is defined as part of the [Composer], which has a [Version] as
/// property 'appVersion'.  The [Schema] carries the same [appVersion] because
/// it is used outside of the composer for server code generation.  The schema
/// [version] itself is a simple incrementing number used primarily to avoid
/// generating unnecessary server code changes in response to Composer changes
/// which have no impact on the schema.
///
/// When the Composer extracts its [AppSchema], it creates a [Schema] with
/// all its component parts also converted to their [Final] form.
///
/// A [Schema] is therefore defined for a [DatasourceInstanceFinal]
///
///
class Schema implements Final{

  const Schema({
    required this.appVersion,
    required this.documents,
    required this.dataInstance,
  });
  final IMap<String,DocumentFinal> documents;
  final DatasourceInstanceFinal dataInstance;
  final Version appVersion;

  /// Compares two versions of a [Schema] without considering the versions.
  /// This enables the server code generator to determine whether changes are
  /// required
  int compareWithoutVersions(Schema other) {
    throw UnimplementedError('compareWithoutVersions');
  }
}
