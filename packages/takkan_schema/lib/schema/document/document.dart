import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:takkan_common/common/data.dart';
import 'package:takkan_common/common/exception.dart';
import 'package:takkan_common/common/inherited.dart';
import 'package:takkan_common/common/log.dart';
import 'package:takkan_common/common/serial.dart';
import 'package:takkan_common/common/walker.dart';

import '../../data/select/condition.dart';
import '../field/field.dart';
import '../query/query.dart';
import 'method.dart';
import 'permissions.dart';

part 'document.g.dart';

/// A [standard] document has no special attributes.
/// A [versioned] document has a simple integer version number, incremented each time
/// it is saved by the user.  This is separate from any auto-save functionality that may be added
/// later
///
/// Implementation of these rules is managed by [DataProvider]
enum DocumentType { standard, versioned }

/// This defines a schema for a 'Class' in Back4App
///
/// A [Document] definition is used by client to control what is displayed to the
/// user, limiting what is presented according to its [Permissions].
///
/// - [name] is the type name of the document, equivalent to a Class in Back4App
///
/// - [isReadOnly] is used to control whether an edit option is presented. A value
/// set here is cascaded to all [fields]
///
/// - [fields] are used by the client, along with data bindings and conversion
/// to connect the presentation layer to the data layer.  [fields] can define
/// validation rules, which are used by the client, but also by the schema
/// generator to generate server side validation.  That way, even if your
/// client-side validation is hacked, server side validation will still be present.
///
/// [permissions] provide role based access control
///
/// [queries] relate to this specific document type (Back4App Class)
///
/// [dataInstance] identifies the source of the data, original identified by
/// *takkan.json*
///
/// An ACL is used only by the schema generator but does need to be defined,
/// with appropriate default. https://gitlab.com/takkan/takkan_script/-/issues/50
///
///
/// Note that [Document] does not implement [HasFinal] even though it has a
/// [DocumentFinal] associated with it.  This is because the construction of
/// final documents is managed by the [DocumentLibrary] to ensure that there is
/// only one instance of the final document.
@JsonSerializable(explicitToJson: true, includeIfNull: false)
class Document with Walkable implements Serializable {
  Document({
    required this.fields,
    required this.className,
    this.documentType = DocumentType.standard,
    Permissions? permissions,
    this.queries = const {},
  }) : permissions = permissions ?? Permissions();

  factory Document.fromJson(Map<String, dynamic> json) =>
      _$DocumentFromJson(json);

  final Map<String, Query> queries;

  final Permissions permissions;
  final DocumentType documentType;
  @FieldJsonConverter()
  final Map<String, Field<dynamic>> fields;
  final String className;

  @JsonKey(includeToJson: false, includeFromJson: false)
  bool get hasValidation {
    for (final Field<dynamic> f in fields.values) {
      if (f.hasValidation) {
        return true;
      }
    }
    return false;
  }

  @override
  Map<String, dynamic> toJson() => _$DocumentToJson(this);

  @JsonKey(includeToJson: false, includeFromJson: false)
  @override
  List<Object> get subElements => [fields, queries, permissions];

  Field<dynamic> field(String fieldName) {
    final f = fields[fieldName];
    if (f == null) {
      final f1 = reservedField(fieldName);
      if (f1 == null) {
        final String msg =
            'Field $fieldName does not exist in document $className';
        logType(runtimeType).e(msg);
        throw TakkanException(msg);
      }
      return f1;
    }
    return f;
  }

  /// see https://gitlab.com/dsowerby/takkan/-/issues/9
  Field<dynamic>? reservedField(String fieldName) {
    switch (fieldName) {
      case 'objectId':
        return Field<String>(property: 'objectId');
    }
    return null;
  }

  Query query(String queryName) {
    final q = queries[queryName];
    if (q == null) {
      final String msg = 'Unknown query with name $queryName';
      logType(runtimeType).e(msg);
      throw TakkanException(msg);
    }
    return q;
  }

  Q operator [](String fieldName) {
    final Field<dynamic>? f = fields[fieldName];
    if (f != null) {
      return Q(fieldName);
    }
    final String msg = '$fieldName is not a valid field name';
    logType(runtimeType).e(msg);
    throw TakkanException(msg);
  }

  bool requiresAuthentication(Method method) {
    return permissions.requiresAuthentication(method);
  }
}

///  - [property] is used where this is a sub-document, typically along with a
/// pointer
///
///  - [className] is the Back4App class name and is therefore immutable once
///  the schema has been created on the server side
class DocumentFinal implements Final {
  const DocumentFinal({
    required this.hasValidation,
    required this.className,
    required this.fields,
    required this.datasourceInstance,
  });

  final DatasourceInstanceFinal datasourceInstance;
  final String className;
  final IMap<String, FieldFinal<dynamic>> fields;
  final bool hasValidation;
}
