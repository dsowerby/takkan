import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:takkan_common/common/data.dart';
import 'package:takkan_common/common/exception.dart';
import 'package:takkan_common/common/version.dart';

import '../app/app_schema.dart';
import '../field/field.dart';
import '../schema.dart';
import 'document.dart';

/// Used only in the process of building the final set of documents.
/// Maintains a map of [IDocument]s during the build process, and then produces
/// their final versions prior to the construction of the [Schema].
///
/// Both are stored per [DatasourceInstance].
class DocumentLibrary {
  DocumentLibrary();

  final Map<DatasourceInstance, Map<String, Document>> _documents = {};
  final Map<DatasourceInstanceFinal, Map<String, DocumentFinal>>
      _finalDocuments = {};
  final Map<Datasource, DatasourceFinal> datasourceMap = {};
  final Map<DatasourceInstance, DatasourceInstanceFinal> instanceMap = {};

  DocumentFinal documentFinal(DatasourceInstance instance, String className) {
    throw UnimplementedError();
  }

  Document document(DatasourceInstance instance, String className) {
    final instanceLibrary = _documents[instance];
    if (instanceLibrary == null) {
      return throw const TakkanException('Unknown datasource instance');
    }
    final document = instanceLibrary[className];
    if (document == null) {
      return throw const TakkanException('Unknown document');
    }
    return document;
  }

  void _makeFinal() {
    _finalDocuments.clear();
    _setupMappings();
    for (final instance in _documents.keys) {
      final instanceDocuments = _documents[instance]!;
      instanceDocuments.forEach((className, document) {
        final instanceFinal= instanceMap[instance]!;
        _finalDocuments[instanceFinal] ??= <String, DocumentFinal>{};
        final DocumentFinal documentFinal = DocumentFinal(
          hasValidation: document.hasValidation,
          className: className,
          fields: finalFields(document),
          datasourceInstance: instanceFinal,
        );
        _finalDocuments[instanceFinal]![className] = documentFinal;
      });
    }
  }

  /// We want to ensure that only one instance of all [Final] implementations
  /// are used.  To do that, we need to set up the mappings between, for example,
  /// [Datasource] and [DatasourceFinal] to then look up the [Final] implementation
  /// rather than reconstruct them.
  void _setupMappings() {
    instanceMap.clear();
    datasourceMap.clear();
    for (final instance in _documents.keys) {
      final datasourceFinal = instance.datasource.asFinal;
      final instanceFinal = DatasourceInstanceFinal(
        datasource: instance.datasource.asFinal,
        instance: instance.instance,
      );
      datasourceMap.putIfAbsent(instance.datasource, () => datasourceFinal);
      instanceMap.putIfAbsent(instance, () => instanceFinal);
    }
  }

  IMap<String, FieldFinal<dynamic>> finalFields(Document document) {
    final Map<String, FieldFinal<dynamic>> finals = {};
    document.fields.forEach((key, value) {
      finals[key] = value.asFinal;
    });
    return IMap<String, FieldFinal<dynamic>>(finals);
  }

  /// Extracts the final content of the library as an [AppSchema], attaching
  /// the version to it.
  AppSchema toAppSchema({required Version appVersion}) {
    _makeFinal();
    final instanceSchemas = <DatasourceInstanceFinal, Schema>{};
    _finalDocuments.forEach((instance, documents) {
      instanceSchemas[instance] = Schema(
        appVersion: appVersion,
        dataInstance: instance,
        documents: IMap(documents),
      );
    });
    return AppSchema(
        instanceSchemas: instanceSchemas, appVersion: appVersion);
  }

  void addDocument(Document document, DatasourceInstance instance) {
    final instanceLibrary = _documents.putIfAbsent(
        instance, () => <String, Document>{});
    instanceLibrary[document.className] = document;
  }
}
