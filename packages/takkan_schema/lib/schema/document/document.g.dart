// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'document.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Document _$DocumentFromJson(Map<String, dynamic> json) => Document(
      fields: (json['fields'] as Map<String, dynamic>).map(
        (k, e) => MapEntry(
            k, const FieldJsonConverter().fromJson(e as Map<String, dynamic>)),
      ),
      className: json['className'] as String,
      documentType:
          $enumDecodeNullable(_$DocumentTypeEnumMap, json['documentType']) ??
              DocumentType.standard,
      permissions: json['permissions'] == null
          ? null
          : Permissions.fromJson(json['permissions'] as Map<String, dynamic>),
      queries: (json['queries'] as Map<String, dynamic>?)?.map(
            (k, e) => MapEntry(k, Query.fromJson(e as Map<String, dynamic>)),
          ) ??
          const {},
    );

Map<String, dynamic> _$DocumentToJson(Document instance) => <String, dynamic>{
      'queries': instance.queries.map((k, e) => MapEntry(k, e.toJson())),
      'permissions': instance.permissions.toJson(),
      'documentType': _$DocumentTypeEnumMap[instance.documentType]!,
      'fields': instance.fields
          .map((k, e) => MapEntry(k, const FieldJsonConverter().toJson(e))),
      'className': instance.className,
    };

const _$DocumentTypeEnumMap = {
  DocumentType.standard: 'standard',
  DocumentType.versioned: 'versioned',
};
