enum Method {
  all,
  read,
  get,
  find,
  count,
  write,
  create,
  update,
  delete,
  addField,
}

extension MethodExtension on Method {
  static Set<Method> basicMethods = {
    Method.count,
    Method.create,
    Method.delete,
    Method.find,
    Method.get,
    Method.update,
    Method.addField,
  };
  static Set<Method> readMethods = {
    Method.count,
    Method.find,
    Method.get,
  };
  static Set<Method> writeMethods = {
    Method.create,
    Method.delete,
    Method.update,
    Method.addField,
  };

  bool get isBasicMethod => basicMethods.contains(this);

  bool get isNotBasicMethod => !isBasicMethod;

  Set<Method> get groupMembers {
    switch (this) {
      case Method.all:
        return MethodExtension.basicMethods;
      case Method.read:
        return MethodExtension.readMethods;
      case Method.write:
        return MethodExtension.writeMethods;
      case Method.get:
      case Method.find:
      case Method.count:
      case Method.create:
      case Method.update:
      case Method.delete:
      case Method.addField:
        return {this};
    }
  }
}


enum MethodFinal {
  get,
  find,
  count,
  create,
  update,
  delete,
  addField,
}