// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'permissions.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Permissions _$PermissionsFromJson(Map<String, dynamic> json) => Permissions(
      permissions: (json['permissions'] as Map<String, dynamic>?)?.map(
            (k, e) => MapEntry($enumDecode(_$MethodEnumMap, k),
                (e as List<dynamic>).map((e) => e as String).toList()),
          ) ??
          const {},
    );

Map<String, dynamic> _$PermissionsToJson(Permissions instance) =>
    <String, dynamic>{
      'permissions': instance.permissions
          .map((k, e) => MapEntry(_$MethodEnumMap[k]!, e.toList())),
    };

const _$MethodEnumMap = {
  Method.all: 'all',
  Method.read: 'read',
  Method.get: 'get',
  Method.find: 'find',
  Method.count: 'count',
  Method.write: 'write',
  Method.create: 'create',
  Method.update: 'update',
  Method.delete: 'delete',
  Method.addField: 'addField',
};

PermissionsDiff _$PermissionsDiffFromJson(Map<String, dynamic> json) =>
    PermissionsDiff(
      addRoles: (json['addRoles'] as Map<String, dynamic>?)?.map(
        (k, e) => MapEntry($enumDecode(_$MethodEnumMap, k),
            (e as List<dynamic>).map((e) => e as String).toList()),
      ),
      removeRoles: (json['removeRoles'] as Map<String, dynamic>?)?.map(
        (k, e) => MapEntry($enumDecode(_$MethodEnumMap, k),
            (e as List<dynamic>).map((e) => e as String).toList()),
      ),
    );

Map<String, dynamic> _$PermissionsDiffToJson(PermissionsDiff instance) =>
    <String, dynamic>{
      'addRoles':
          instance.addRoles?.map((k, e) => MapEntry(_$MethodEnumMap[k]!, e)),
      'removeRoles':
          instance.removeRoles?.map((k, e) => MapEntry(_$MethodEnumMap[k]!, e)),
    };
