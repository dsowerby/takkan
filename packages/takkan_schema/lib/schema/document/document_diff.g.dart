// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'document_diff.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DocumentDiff _$DocumentDiffFromJson(Map<String, dynamic> json) => DocumentDiff(
      addFields: (json['addFields'] as Map<String, dynamic>?)?.map(
            (k, e) => MapEntry(k,
                const FieldJsonConverter().fromJson(e as Map<String, dynamic>)),
          ) ??
          const {},
      removeFields: (json['removeFields'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      amendFields: (json['amendFields'] as Map<String, dynamic>?)?.map(
            (k, e) => MapEntry(
                k, FieldDiff<dynamic>.fromJson(e as Map<String, dynamic>)),
          ) ??
          const {},
      permissions: json['permissions'] == null
          ? null
          : PermissionsDiff.fromJson(
              json['permissions'] as Map<String, dynamic>),
      addQueries: (json['addQueries'] as Map<String, dynamic>?)?.map(
            (k, e) => MapEntry(k, Query.fromJson(e as Map<String, dynamic>)),
          ) ??
          const {},
      removeQueries: (json['removeQueries'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      amendQueries: (json['amendQueries'] as Map<String, dynamic>?)?.map(
            (k, e) =>
                MapEntry(k, QueryDiff.fromJson(e as Map<String, dynamic>)),
          ) ??
          const {},
    );

Map<String, dynamic> _$DocumentDiffToJson(DocumentDiff instance) =>
    <String, dynamic>{
      'permissions': instance.permissions?.toJson(),
      'addQueries': instance.addQueries.map((k, e) => MapEntry(k, e.toJson())),
      'addFields': instance.addFields
          .map((k, e) => MapEntry(k, const FieldJsonConverter().toJson(e))),
      'removeFields': instance.removeFields,
      'removeQueries': instance.removeQueries,
      'amendFields':
          instance.amendFields.map((k, e) => MapEntry(k, e.toJson())),
      'amendQueries':
          instance.amendQueries.map((k, e) => MapEntry(k, e.toJson())),
    };
