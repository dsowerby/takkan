import 'package:json_annotation/json_annotation.dart';

import '../common/diff.dart';
import '../field/field.dart';
import '../field/field_diff.dart';
import '../query/query.dart';
import 'document.dart';
import 'permissions.dart';

part 'document_diff.g.dart';

@JsonSerializable(explicitToJson: true)
@FieldJsonConverter()
class DocumentDiff with DiffUpdate implements Diff<Document> {
  const DocumentDiff({
    this.addFields = const {},
    this.removeFields = const [],
    this.amendFields = const {},
    this.permissions,
    this.addQueries = const {},
    this.removeQueries = const [],
    this.amendQueries = const {},
  });

  factory DocumentDiff.fromJson(Map<String, dynamic> json) =>
      _$DocumentDiffFromJson(json);

  final PermissionsDiff? permissions;

  final Map<String, Query> addQueries;
  final Map<String, Field<dynamic>> addFields;
  final List<String> removeFields;
  final List<String> removeQueries;
  final Map<String, FieldDiff<dynamic>> amendFields;
  final Map<String, QueryDiff> amendQueries;

  Map<String, dynamic> toJson() => _$DocumentDiffToJson(this);

  /// Returns a new [Document] with the given [documentDiff] applied.
  ///
  /// Changes to fields are applied in this order:
  ///  - amend
  ///  - remove
  ///  - add
  ///
  /// This means that it is possible, with a poorly defined [base], to remove a
  /// field and add it back in, or to amend a field and then remove it,
  /// all within one [base].
  ///
  /// Changes to [permissions] are always amendments
  @override
  Document applyTo(Document base) {
    return Document(className: base.className, // TODO: Cannot really support class change
      fields: _updatedFields(base),
      queries: _updatedQueries(base),
      permissions: (permissions == null)
          ? base.permissions
          : permissions!.applyTo(base.permissions),
    );
  }

  Map<String, Query> _updatedQueries(Document base) {
    return updateSubElements(
      baseSubElements: base.queries,
      removeSubElements: removeQueries,
      amendSubElements: amendQueries,
      addSubElements: addQueries,
    );
  }

  Map<String, Field<dynamic>> _updatedFields(Document base) {
    return updateSubElements(
      baseSubElements: base.fields,
      removeSubElements: removeFields,
      amendSubElements: amendFields,
      addSubElements: addFields,
    );
  }
}
