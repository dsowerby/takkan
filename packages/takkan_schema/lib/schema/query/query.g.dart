// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'query.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Query _$QueryFromJson(Map<String, dynamic> json) => Query(
      constraints: (json['constraints'] as List<dynamic>?)
              ?.map((e) => Condition<dynamic, dynamic>.fromJson(
                  e as Map<String, dynamic>))
              .toList() ??
          const [],
      params: (json['params'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      queryScript: json['queryScript'] as String?,
      returnSingle: json['returnSingle'] as bool? ?? false,
      useStream: json['useStream'] as bool? ?? false,
    );

Map<String, dynamic> _$QueryToJson(Query instance) => <String, dynamic>{
      'queryScript': instance.queryScript,
      'params': instance.params,
      'returnSingle': instance.returnSingle,
      'useStream': instance.useStream,
      'constraints': instance.constraints.map((e) => e.toJson()).toList(),
    };

QueryDiff _$QueryDiffFromJson(Map<String, dynamic> json) => QueryDiff(
      constraints: (json['constraints'] as List<dynamic>?)
          ?.map((e) =>
              Condition<dynamic, dynamic>.fromJson(e as Map<String, dynamic>))
          .toList(),
      params:
          (json['params'] as List<dynamic>?)?.map((e) => e as String).toList(),
      queryScript: json['queryScript'] as String?,
      returnSingle: json['returnSingle'] as bool?,
      useStream: json['useStream'] as bool?,
      removeQueryScript: json['removeQueryScript'] as bool? ?? false,
    );

Map<String, dynamic> _$QueryDiffToJson(QueryDiff instance) => <String, dynamic>{
      'queryScript': instance.queryScript,
      'params': instance.params,
      'returnSingle': instance.returnSingle,
      'useStream': instance.useStream,
      'removeQueryScript': instance.removeQueryScript,
      'constraints': instance.constraints?.map((e) => e.toJson()).toList(),
    };
