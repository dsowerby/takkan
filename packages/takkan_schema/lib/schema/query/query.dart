import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:takkan_common/common/inherited.dart';

import '../../data/select/condition.dart';
import '../../data/select/condition_final.dart';
import '../common/diff.dart';
import '../document/document.dart';
import '../../data/select/expression.dart';

part 'query.g.dart';

/// A [Query] can be defined using either a [queryScript] or set of [constraints],
/// or even both as they are combined in the creation of [QueryFinal].
///
/// The Back4App control fields (objectId,updatedAt,createdAt) should not need to
/// be declared,but this is an open issue:
///
/// TODO:  https://gitlab.com/dsowerby/takkan/-/issues/7
///
/// If [returnSingle] is true, the query must return exactly one document.
/// if false, the query returns a 0..n List of Documents
///
/// If [useStream] is true results are returned as a Stream, otherwise as a simple
/// Future. [NOT YET IMPLEMENTED - ALWAYS A FUTURE CURRENTLY] TODO:https://gitlab.com/dsowerby/takkan/-/issues/170
///
///
/// [params] are not yet implemented - TODO: https://gitlab.com/takkan/takkan_design/-/issues/47
@JsonSerializable(explicitToJson: true)
class Query  implements HasFinal<QueryFinal> {
  Query({
    this.constraints = const [],
    this.params = const [],
    this.queryScript,
    this.returnSingle = false,
    this.useStream = false,
  });

  factory Query.fromJson(Map<String, dynamic> json) => _$QueryFromJson(json);

  final String? queryScript;
  final List<String> params;
  final bool returnSingle;
  final bool useStream;

  Document get document => throw UnimplementedError();

  /// To retrieve conditions specified by [queryScript] as well,
  /// use [conditions]
  final List<Condition<dynamic, dynamic>> constraints;

  @JsonKey(includeToJson: false, includeFromJson: false)
  final List<Condition<dynamic, dynamic>> _conditions = [];

  /// Combines [constraints] with [queryScript].  Use this to ensure you have all
  /// conditions
  @JsonKey(includeToJson: false, includeFromJson: false)
  List<Condition<dynamic, dynamic>> get conditions => _conditions;

  @override
  Map<String, dynamic> toJson() => _$QueryToJson(this);

  void buildConditions() {
    _conditions.clear();
    for (final condition in constraints) {
      _conditions.add(condition);
    }
    final script = queryScript;
    if (script != null) {
      final scriptConditions = const ExpressionConverter()
          .parseConditions(script: script, forQuery: true);
      _conditions.addAll(scriptConditions);
    }
  }

  @override
  QueryFinal get asFinal {
    buildConditions();
    return QueryFinal(
      constraints: IList(conditions.map((e) => e.asFinal)),
      returnSingle: returnSingle,
      params: params,
      useStream: useStream,
    );
  }
}

@JsonSerializable(explicitToJson: true)
class QueryDiff implements Diff<Query> {
  const QueryDiff({
    this.constraints,
    this.params,
    this.queryScript,
    this.returnSingle,
    this.useStream,
    this.removeQueryScript = false,
  });

  factory QueryDiff.fromJson(Map<String, dynamic> json) =>
      _$QueryDiffFromJson(json);

  final String? queryScript;
  final List<String>? params;
  final bool? returnSingle;
  final bool? useStream;
  final bool removeQueryScript;

  final List<Condition<dynamic, dynamic>>? constraints;

  Map<String, dynamic> toJson() => _$QueryDiffToJson(this);

  @override
  Query applyTo(Query base) {
    return Query(
      queryScript: removeQueryScript ? null : queryScript ?? base.queryScript,
      params: params ?? base.params,
      returnSingle: returnSingle ?? base.returnSingle,
      useStream: useStream ?? base.useStream,
      constraints: constraints ?? base.constraints,
    );
  }
}

class QueryFinal implements Final {
  const QueryFinal({
    required this.constraints,
    required this.returnSingle,
    this.useStream = false,
    this.params = const [],
  });

  final IList<ConditionFinal<dynamic>> constraints;
  final bool returnSingle;
  final bool useStream;
  final List<String> params;
}
