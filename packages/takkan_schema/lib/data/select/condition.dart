// ignore_for_file: avoid_classes_with_only_static_members

import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:takkan_common/common/serial.dart';

import 'condition_final.dart';
import 'integer_condition.dart';
import 'operation.dart';
import 'string_condition.dart';

part 'condition.g.dart';

@JsonSerializable(explicitToJson: true)
class Condition<MODEL, OPERAND> implements Serializable {
  const Condition({
    required this.fieldName,
    required this.operator,
    required this.operand,
    required this.forQuery,
  });

  factory Condition.fromJson(Map<String, dynamic> json) =>
      _$ConditionFromJson(json);

  final String fieldName;
  final Operator operator;
  final dynamic operand;
  final bool forQuery;

  ConditionFinal<MODEL> get asFinal {
    return ConditionFinal<MODEL>(
      forQuery: forQuery,
      fieldName: fieldName,
      operator: operator,
      operand: operand,
      operation: operationBuilder(operator),
    );
  }


  @override
  String toString() {
    return jsonEncode(toJson());
  }

  @override
  Map<String, dynamic> toJson() => _$ConditionToJson(this);

  Operation<MODEL, dynamic> operationBuilder(Operator operator) {
    switch (MODEL) {
      case int:
        return IntegerConditionBuilder(forQuery: forQuery, fieldName: fieldName)
            .operation(operator, operand) as Operation<MODEL, dynamic>;
      case String:
        return StringConditionBuilder(forQuery: forQuery, fieldName: fieldName)
            .operation(operator, operand) as Operation<MODEL, dynamic>;
      default:
        throw UnsupportedError(
            'Operator $operator is not supported for ${MODEL.runtimeType}');
    }
  }
}

/// This is a static only class to allow easier construction of validation
/// constraints in [Field.constraints].
///
/// It should always mirror the getters in [Q]
///
/// The field name in the returned builder is set to an empty String, because
/// validation is applied to a specific field.  The name is added later during
/// This is adjusted by the validation building process in [Field.doInit]
class V {
  static IntegerConditionBuilder get int =>
      IntegerConditionBuilder(fieldName: '', forQuery: false);

  static StringConditionBuilder get string =>
      StringConditionBuilder(fieldName: '', forQuery: false);
}

/// A [ConditionBuilder] for use in [Query] definitions, with name abbreviated
/// for brevity
class Q {
  Q(this.fieldName);

  final String fieldName;

  IntegerConditionBuilder get int =>
      IntegerConditionBuilder(fieldName: fieldName, forQuery: true);

  StringConditionBuilder get string =>
      StringConditionBuilder(fieldName: fieldName, forQuery: true);
}
