// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'condition.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Condition<MODEL, OPERAND> _$ConditionFromJson<MODEL, OPERAND>(
        Map<String, dynamic> json) =>
    Condition<MODEL, OPERAND>(
      fieldName: json['fieldName'] as String,
      operator: $enumDecode(_$OperatorEnumMap, json['operator']),
      operand: json['operand'],
      forQuery: json['forQuery'] as bool,
    );

Map<String, dynamic> _$ConditionToJson<MODEL, OPERAND>(
        Condition<MODEL, OPERAND> instance) =>
    <String, dynamic>{
      'fieldName': instance.fieldName,
      'operator': _$OperatorEnumMap[instance.operator]!,
      'operand': instance.operand,
      'forQuery': instance.forQuery,
    };

const _$OperatorEnumMap = {
  Operator.contains: 'contains',
  Operator.doesNotContain: 'doesNotContain',
  Operator.equalTo: 'equalTo',
  Operator.notEqualTo: 'notEqualTo',
  Operator.greaterThan: 'greaterThan',
  Operator.greaterThanOrEqualTo: 'greaterThanOrEqualTo',
  Operator.lessThan: 'lessThan',
  Operator.lessThanOrEqualTo: 'lessThanOrEqualTo',
  Operator.isNotNull: 'isNotNull',
  Operator.isNull: 'isNull',
  Operator.lengthEqualTo: 'lengthEqualTo',
  Operator.lengthNotEqualTo: 'lengthNotEqualTo',
  Operator.lengthGreaterThan: 'lengthGreaterThan',
  Operator.lengthGreaterThanOrEqualTo: 'lengthGreaterThanOrEqualTo',
  Operator.lengthLessThan: 'lengthLessThan',
  Operator.lengthLessThanOrEqualTo: 'lengthLessThanOrEqualTo',
};
