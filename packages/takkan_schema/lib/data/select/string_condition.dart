
import 'condition.dart';
import 'condition_builder.dart';
import 'operation.dart';

/// The only real purpose of this class is to support IDE friendly definitions of
/// query and validation conditions, using [Q] and [V] respectively.
///
/// The [fieldName] cannot be final as it may need updating when used in validation
class StringConditionBuilder extends ConditionBuilder<String> {
  StringConditionBuilder({
    required super.fieldName,
    required super.forQuery,
  }) : super(ref: '');

  Condition<String, String> equalTo(String operand) {
    return basicCondition(Operator.equalTo, operand);
  }

  Condition<String, String> notEqualTo(dynamic operand) {
    return basicCondition(Operator.notEqualTo, operand);
  }

  Condition<String, dynamic> lengthEqualTo(int operand) {
    return condition(Operator.lengthEqualTo, operand);
  }

  Condition<String, dynamic> lengthNotEqualTo(int operand) {
    return condition(Operator.lengthNotEqualTo, operand);
  }

  Condition<String,dynamic> lengthLessThan(int operand) {
    return condition(Operator.lengthLessThan, operand);
  }

  Condition<String,dynamic> lengthLessThanOrEqualTo(int operand) {
    return condition(Operator.lengthLessThanOrEqualTo, operand);
  }

  Condition<String,dynamic> lengthGreaterThan(int operand) {
    return condition(Operator.lengthGreaterThan, operand);
  }

  Condition<String,dynamic> lengthGreaterThanOrEqualTo(int operand) {
    return condition(Operator.lengthGreaterThanOrEqualTo, operand);
  }

  Condition<String,dynamic> contains(String operand) {
    return condition(Operator.contains, operand);
  }

  Condition<String,dynamic> doesNotContain(String operand) {
    return condition(Operator.doesNotContain, operand);
  }



  @override
  OperationFunction<String,String> compareOperation(Operator operator) {
    throw UnsupportedError(
        'Operator $operator not supported in StringConditionBuilder');
  }

  @override
  OperationFunction<String,int> lengthOperation(Operator operator) {
    switch (operator) {
      case Operator.lengthEqualTo:
        return (value, operand) => value.length == operand;
      case Operator.lengthNotEqualTo:
        return (value, operand) => value.length != operand;
      case Operator.lengthLessThan:
        return (value, operand) => value.length < operand;
      case Operator.lengthLessThanOrEqualTo:
        return (value, operand) => value.length <= operand;
      case Operator.lengthGreaterThan:
        return (value, operand) => value.length > operand;
      case Operator.lengthGreaterThanOrEqualTo:
        return (value, operand) => value.length >= operand;
      default:
        throw UnsupportedError(
            'Operator $operator not supported in lengthOperation');
    }
  }

  @override
  OperationFunction<String,String> containsOperation(Operator operator) {
    switch (operator) {
      case Operator.contains:
        return (value, operand) => value.contains(operand);
      case Operator.doesNotContain:
        return (value, operand) => !value.contains(operand);

      default:
        throw UnsupportedError(
            'Operator $operator not supported in containsOperation');
    }
  }
}
