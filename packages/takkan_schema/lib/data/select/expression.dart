import 'package:characters/characters.dart';
import 'package:takkan_common/common/exception.dart';
import 'package:takkan_common/common/log.dart';

import 'condition.dart';
import 'operation.dart';

const statementSeparator = '&&';

/// Builds a [Condition] instance from an expression in the form of "field operator operand"
///
/// - a > b
/// - a contains b
///
/// where the operator is separated from operands by a single space
///
/// For String operands, surround with single quotes:
///
/// - a contains 'x'
///
class ExpressionConverter {
  const ExpressionConverter();

  List<Condition<MODEL, dynamic>>  parseConditions <MODEL>({required String script,required bool forQuery}) {
    final result=<Condition<MODEL, dynamic>>[];
    final statements=script.split(statementSeparator);
    for(final statement in statements){
      result.add(parseCondition(expression: statement,forQuery: forQuery));
    }
    return result;
  }
  
  Condition<MODEL, dynamic> parseCondition <MODEL>(
      {required String expression,required bool forQuery}) {
    final s = expression.split(' ');

    /// helps with minor spacing issues
    s.removeWhere((element) => element.isEmpty);
    if (s.length != 3) {
      final String msg = '$expression is not a valid expression of a condition';
      logType(runtimeType).e(msg);
      throw TakkanException(msg);
    }
    final String operand1 = s[0].trim();
    final String operatorScript = s[1].trim();
    final String operand2 = s[2].trim();

    final operator = scriptLookup[operatorScript];

    if (operator == null) {
      final String msg = 'Unable to identify operator in condition $expression ';
      logType(runtimeType).e(msg);
      throw TakkanException(msg);
    }

    return Condition(
      fieldName: operand1,
      operator: operator,
      operand: _decodeValue(operand2),
      forQuery: forQuery,
    );
  }

  dynamic _decodeValue(String source) {
    final src = Characters(source);
    final range = src.getRange(0);
    final CharacterRange? singleQuoted = range.findFirst("'".characters);
    if (singleQuoted != null) {
      singleQuoted.moveUntil("'".characters);
      return singleQuoted.currentCharacters.toString();
    }
    final CharacterRange? doubleQuoted = range.findFirst('"'.characters);
    if (doubleQuoted != null) {
      doubleQuoted.moveUntil('"'.characters);
      return doubleQuoted.currentCharacters.toString();
    }
    if (int.tryParse(source) != null) {
      return int.parse(source);
    }
    if (double.tryParse(source) != null) {
      return double.parse(source);
    }
    if (source.toLowerCase() == 'true') {
      return true;
    }
    if (source.toLowerCase() == 'false') {
      return false;
    }
    if (source.toLowerCase() == 'null') {
      return null;
    }
    if (DateTime.tryParse(source) != null) {
      return DateTime.parse(source);
    }
    throw UnimplementedError(
        'value: $source Have you forgotten to put quotes around String in your validation or query script?. If not, see https://gitlab.com/takkan/takkan_script/-/issues/40');
  }

  /// In this context, field names are the property names, which are not translated
  /// as they are usually names within a schema
  ///
  /// Parses a String expression and converts to one or more [Condition]s
  /// Field names are part of the query expression, unlike [parseForValidation],
  /// which relate to a specific field
  // List<Condition<dynamic, dynamic>> parseForQuery(
  //     {required String expression}) {
  //   final List<Condition<dynamic, dynamic>> conditions =
  //       List<Condition<dynamic, dynamic>>.empty(growable: true);
  //   final c = expression.split(statementSeparator);
  //   for (final String cond in c) {
  //     conditions.add(parseCondition(cond, true));
  //   }
  //   return conditions;
  // }

  /// In this context, field names are the property names, which are not translated
  /// as they are usually names within a schema
  ///
  /// Parses a String expression and converts to one or more [Condition]s
  /// The field name for the [Condition] is taken from [field], unlike [parseForQuery],
  /// which contains field names as part of the query expression
  // List<Condition<dynamic, dynamic>> parseForValidation({
  //   required Field<dynamic> field,
  //   required String expression,
  // }) {
  //   final f = field;
  //   final List<Condition<dynamic, dynamic>> conditions =
  //       List<Condition<dynamic, dynamic>>.empty(growable: true);
  //   final c = expression.split(statementSeparator);
  //   for (final String cond in c) {
  //     conditions.add(parseCondition('${f.property} $cond', false));
  //   }
  //   return conditions;
  // }
}
