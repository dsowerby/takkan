import 'package:takkan_common/common/inherited.dart';

import 'operation.dart';

class ConditionFinal<MODEL> implements Final {
  const ConditionFinal({
    required this.operand,
    required this.operation,
    required this.operator,
    required this.fieldName,
    required this.forQuery,
  });

  final bool forQuery;
  final dynamic operand;
  final String fieldName;
  final Operator operator;
  final Operation<MODEL, dynamic> operation;

  /// We handle the null values here so that [operation] can be non-nullable
  bool evaluate(MODEL? value) {
    final v = value;
    if (v == null) {
      if (this.operator == Operator.isNotNull) return false;
      return true;
    } else {
      return operation.evaluate(v);
    }
  }

  // TODO: should this be in Field and Query rather than here?
  String cloudCode(String fieldName) {
    return '${b4aSnippet[operator]!.replaceAll('#n', fieldName).replaceAll('#v', (operand is String) ? '"$operand"' : operand.toString())});';
  }
}
