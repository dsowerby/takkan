import 'condition.dart';
import 'operation.dart';
/// Common base class for building [Condition] instances.  There is a sub-class
/// for each supported data type, because many of the operations are required
/// to be different for each data type
abstract class ConditionBuilder<MODEL> {

  const ConditionBuilder({
    required this.ref,
    required this.fieldName,
    required this.forQuery,
  });
  final MODEL ref;
  final String fieldName;
  final bool forQuery;

  ///  [ref] is passed as the operand only because it is the right type.  We do
  ///  not use it
  Condition<MODEL, MODEL> isNull() {
    return basicCondition(Operator.isNull, ref);
  }

  ///  [ref] is passed as the operand only because it is the right type.  We do
  ///  not use it
  Condition<MODEL, MODEL> isNotNull() {
    return basicCondition(Operator.isNotNull, ref);
  }

  Condition<MODEL, MODEL> basicCondition(Operator operator, dynamic operand) {
    return Condition<MODEL, MODEL>(
      fieldName: fieldName,
      operator: operator,
      operand: operand,
      forQuery: forQuery,
    );
  }

  OperationFunction<MODEL,dynamic> basicOperation(Operator operator) {
    switch (operator) {
      case Operator.equalTo:
        return (value, operand) => value == operand;
      case Operator.notEqualTo:
        return (value, operand) => value != operand;
      case Operator.isNull:
        return (value, operand) => value == null;
      case Operator.isNotNull:
        return (value, operand) => value != null;
      default:
        throw UnsupportedError(
            'Operator $operator not supported in basicOperation');
    }
  }

  OperationFunction<MODEL, MODEL> compareOperation(Operator operator);

  OperationFunction<MODEL, int> lengthOperation(Operator operator) {
    throw UnsupportedError('Operator $operator not supported in $runtimeType');
  }

  OperationFunction<MODEL,MODEL> containsOperation(Operator operator) {
    throw UnsupportedError('Operator $operator not supported in $runtimeType');
  }

  Condition<MODEL, dynamic> condition(Operator operator, dynamic operand) {
    return Condition<MODEL, dynamic>(
      fieldName: fieldName,
      operator: operator,
      operand: operand,
      forQuery: forQuery,
    );
  }

  Operation<MODEL, dynamic> operation(Operator operator, dynamic operand) {
    switch (operator) {
      case Operator.equalTo:
      case Operator.notEqualTo:
      case Operator.isNull:
      case Operator.isNotNull:
        return Operation(operand: operand, operation: basicOperation(operator));
      case Operator.lessThan:
      case Operator.lessThanOrEqualTo:
      case Operator.greaterThan:
      case Operator.greaterThanOrEqualTo:
        return Operation<MODEL,MODEL>(
            operand: operand as MODEL, operation: compareOperation(operator));
      case Operator.lengthEqualTo:
      case Operator.lengthNotEqualTo:
      case Operator.lengthGreaterThan:
      case Operator.lengthGreaterThanOrEqualTo:
      case Operator.lengthLessThan:
      case Operator.lengthLessThanOrEqualTo:
        return Operation<MODEL,int>(
            operand: operand as int, operation: lengthOperation(operator));
      case Operator.contains:
      case Operator.doesNotContain:
        return Operation<MODEL,MODEL>(
            operand: operand as MODEL, operation: containsOperation(operator));

      default:
        throw UnsupportedError(
            'Operator $operator not supported for type ${ref.runtimeType}');
    }
  }
}
