
import 'condition.dart';
import 'condition_builder.dart';
import 'operation.dart';

/// The only real purpose of this class is to support IDE friendly definitions of
/// query and validation conditions, using [Q] and [V] respectively.
///
/// The [fieldName] cannot be final as it may need updating when used in validation
class IntegerConditionBuilder extends ConditionBuilder<int> {
  IntegerConditionBuilder({
    required super.fieldName,
    required super.forQuery,
  }) : super(ref: 0);

  Condition<int, int> equalTo(int operand) {
    return basicCondition(Operator.equalTo, operand);
  }

  Condition<int, int> notEqualTo(dynamic operand) {
    return basicCondition(Operator.notEqualTo, operand);
  }

  Condition<int, dynamic> lessThan(int operand) {
    return condition(Operator.lessThan, operand);
  }

  Condition<int, dynamic> lessThanOrEqualTo(int operand) {
    return condition(Operator.lessThanOrEqualTo, operand);
  }

  Condition<int, dynamic> greaterThan(int operand) {
    return condition(Operator.greaterThan, operand);
  }

  Condition<int, dynamic> greaterThanOrEqualTo(int operand) {
    return condition(Operator.greaterThanOrEqualTo, operand);
  }

  @override
  OperationFunction<int,int> compareOperation(
      Operator operator) {
    switch (operator) {
      case Operator.greaterThan:
        return (value, operand) => value> operand;
      case Operator.greaterThanOrEqualTo:
        return (value, operand) => value>= operand;
      case Operator.lessThan:
        return (value, operand) => value< operand;
      case Operator.lessThanOrEqualTo:
        return (value, operand) => value<= operand;
      default:
        throw UnsupportedError(
            'Operator $operator not supported in IntegerCondition');
    }
  }
}
