import 'condition.dart';

class Operation<MODEL, OPERAND> {

  const Operation({required this.operand, required this.operation});
  final OPERAND operand;
  final OperationFunction<MODEL, OPERAND> operation;

  bool evaluate(MODEL value) {
    return operation(value, operand);
  }
}

typedef OperationFunction<MODEL,OPERAND> = bool Function(MODEL value, OPERAND operand);

/// [Operator] is used in [Condition] for specifying query and validation conditions
///
/// Associated mappings and lookups provide other related information:
///
/// - [operatorScript] provides the syntax for String based expressions
/// - [scriptLookup] is a reverse lookup of [operatorScript], which to translate
/// a String back to an [Operator] constant
/// - [operatorSupportsQuery] identifies which operators are suitable for queries.
/// - [b4aSnippet] is used to generate Back4App code from the [Operator]
///
enum Operator {
  contains,
  doesNotContain,
  equalTo,
  notEqualTo,
  greaterThan,
  greaterThanOrEqualTo,
  lessThan,
  lessThanOrEqualTo,
  isNotNull,
  isNull,
  lengthEqualTo,
  lengthNotEqualTo,
  lengthGreaterThan,
  lengthGreaterThanOrEqualTo,
  lengthLessThan,
  lengthLessThanOrEqualTo,
}

const Map<Operator, List<String>> operatorScript = {
  Operator.contains: ['contains'],
  Operator.doesNotContain: ['doesNotContain', '!contains'],
  Operator.equalTo: ['==', 'equalTo'],
  Operator.notEqualTo: ['!=', 'notEqualTo'],
  Operator.greaterThan: ['>', 'greaterThan'],
  Operator.greaterThanOrEqualTo: ['>=', 'greaterThanOrEqualTo'],
  Operator.lessThan: ['<', 'lessThan'],
  Operator.lessThanOrEqualTo: ['<=', 'lessThanOrEqualTo'],
  Operator.isNotNull: ['isNotNull', '!=null'],
  Operator.isNull: ['isNull', '==null'],
  Operator.lengthEqualTo: ['lengthEqualTo', 'length=='],
  Operator.lengthNotEqualTo: ['lengthNotEqualTo', 'length!='],
  Operator.lengthGreaterThan: ['lengthGreaterThan', 'length>'],
  Operator.lengthGreaterThanOrEqualTo: [
    'lengthGreaterThanOrEqualTo',
    'length>='
  ],
  Operator.lengthLessThan: ['lengthLessThan', 'length<'],
  Operator.lengthLessThanOrEqualTo: ['lengthLessThanOrEqualTo', 'length<='],
};

/// A Javascript code snippet used by the server side code generator for Back4App
///
/// The #n, #v syntax is not ideal, as JavaScript formatting requires space after comma for formatting
/// and this is an odd place to be worrying about that.  Is there a better way?
/// See https://gitlab.com/takkan/takkan_script/-/issues/53
///
/// TODO: Move this to the Back4App generator
const Map<Operator, String> b4aSnippet = {
  Operator.contains: 'contains ("#n", "#v")',
  Operator.doesNotContain: 'doesNotContain ("#n", "#v")',
  Operator.equalTo: 'equalTo("#n", #v")',
  Operator.notEqualTo: 'notEqualTo("#n", #v")',
  Operator.greaterThan: 'greaterThan("#n", #v")',
  Operator.greaterThanOrEqualTo: 'greaterThanOrEqualTo("#n", #v")',
  Operator.lessThan: 'lessThan("#n", #v")',
  Operator.lessThanOrEqualTo: 'lessThanOrEqualTo("#n", #v")',
  Operator.isNotNull: 'isNotNull("#n")',
  Operator.isNull: 'isNull("#n")',
  Operator.lengthEqualTo: 'lengthEqualTo("#n", #v")',
  Operator.lengthNotEqualTo: 'lengthNotEqualTo("#n", #v")',
  Operator.lengthGreaterThan: 'lengthGreaterThan("#n", #v")',
  Operator.lengthGreaterThanOrEqualTo: 'lengthGreaterThanOrEqualTo("#n", #v")',
  Operator.lengthLessThan: 'lengthLessThan("#n", #v")',
  Operator.lengthLessThanOrEqualTo: 'lengthLessThanOrEqualTo("#n", #v")',
};

const scriptLookup = {
  'contains': Operator.contains,
  'doesNotContain': Operator.doesNotContain,
  '!contains': Operator.doesNotContain,
  '==': Operator.equalTo,
  'equalTo': Operator.equalTo,
  '!=': Operator.notEqualTo,
  'notEqualTo': Operator.notEqualTo,
  '>': Operator.greaterThan,
  'greaterThan': Operator.greaterThan,
  '>=': Operator.greaterThanOrEqualTo,
  'greaterThanOrEqualTo': Operator.greaterThanOrEqualTo,
  '<': Operator.lessThan,
  'lessThan': Operator.lessThan,
  '<=': Operator.lessThanOrEqualTo,
  'lessThanOrEqualTo': Operator.lessThanOrEqualTo,
  'isNotNull': Operator.isNotNull,
  '!=null': Operator.isNotNull,
  'isNull': Operator.isNull,
  '==null': Operator.isNull,
  'lengthEqualTo': Operator.lengthEqualTo,
  'length==': Operator.lengthEqualTo,
  'lengthNotEqualTo': Operator.lengthNotEqualTo,
  'length!=': Operator.lengthNotEqualTo,
  'lengthGreaterThan': Operator.lengthGreaterThan,
  'length>': Operator.lengthGreaterThan,
  'lengthGreaterThanOrEqualTo': Operator.lengthGreaterThanOrEqualTo,
  'length>=': Operator.lengthGreaterThanOrEqualTo,
  'lengthLessThan': Operator.lengthLessThan,
  'length<': Operator.lengthLessThan,
  'lengthLessThanOrEqualTo': Operator.lengthLessThanOrEqualTo,
  'length<=': Operator.lengthLessThanOrEqualTo,
};

/// Not all conditions are suitable for queries, and this is determined by the Back4App
/// Parse.Query.  For example, Parse.Query has no option for selecting String fields
/// on the basis of String length.
bool operatorSupportsQuery(Operator operator) {
  switch (operator) {
    case Operator.lengthGreaterThanOrEqualTo:
    case Operator.lengthLessThanOrEqualTo:
    case Operator.lengthEqualTo:
    case Operator.lengthNotEqualTo:
    case Operator.lengthGreaterThan:
    case Operator.lengthLessThan:
      return false;
    default:
      return true;
  }
}
