// ignore_for_file: avoid_implementing_value_types

import 'package:mocktail/mocktail.dart';
import 'package:takkan_common/common/data.dart';
import 'package:takkan_common/common/exception.dart';
import 'package:takkan_schema/data/select/condition.dart';
import 'package:takkan_schema/schema/document/document.dart';
import 'package:takkan_schema/schema/document/permissions.dart';
import 'package:takkan_schema/schema/field/field.dart';
import 'package:takkan_schema/schema/field/field_diff.dart';
import 'package:test/test.dart';

Matcher throwsTakkanException = throwsA(isA<TakkanException>());

// ignore: strict_raw_type
class MockField extends Mock implements Field {}

class MockFieldFinal<T> extends Mock implements FieldFinal<T> {}

// ignore: strict_raw_type
class MockFieldDiff extends Mock implements FieldDiff {}

class MockDocument extends Mock implements Document {}

class MockPermissions extends Mock implements Permissions {}

class MockDatasourceInstance extends Mock implements DatasourceInstance {}

class MockDatasourceInstanceFinal extends Mock
    implements DatasourceInstanceFinal {}

class MockDocumentFinal extends Mock implements DocumentFinal {}

/// To use in tests:
/// - expect(a, ConditionMatcher(b));
//  - expect(actual, containsAll(expected.map(ConditionMatcher.new)));
class ConditionMatcher<M, O> extends Matcher {
  ConditionMatcher(this.expected);

  final Condition<M, O> expected;
  late Condition<M, O> actual;

  @override
  // ignore: strict_raw_type
  bool matches(item, Map matchState) {
    if (item is! Condition<M, O>) {
      return false;
    }
    actual = item;
    final condition = item;
    return condition.fieldName == expected.fieldName &&
        condition.operator == expected.operator &&
        condition.operand == expected.operand &&
        condition.forQuery == expected.forQuery;
  }

  @override
  Description describe(Description description) {
    description.add(expected.runtimeType.toString());
    description.add(':<');
    description.add(expected.toString());
    description.add('>');
    return description;
  }
}
