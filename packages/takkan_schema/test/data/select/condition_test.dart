import 'package:takkan_schema/data/select/condition.dart';
import 'package:test/test.dart';

void main() {
  group('Condition', () {
    setUpAll(() {});

    tearDownAll(() {});

    setUp(() {});

    tearDown(() {});

    group('int', () {
      test('equalTo', () {
        // given
        final c1 = V.int.equalTo(5);
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate(5), true);
        expect(fin.evaluate(6), false);
      });
      test('notEqualTo', () {
        // given
        final c1 = V.int.notEqualTo(5);
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate(5), false);
        expect(fin.evaluate(6), true);
      });
      test('isNull', () {
        // given
        final c1 = V.int.isNull();
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate(null), true);
        expect(fin.evaluate(6), false);
      });
      test('isNotNull', () {
        // given
        final c1 = V.int.isNotNull();
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate(null), false);
        expect(fin.evaluate(6), true);
      });
      test('isLessThan', () {
        // given
        final c1 = V.int.lessThan(5);
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate(5), false);
        expect(fin.evaluate(4), true);
      });
      test('isLessThanOrEqualTo', () {
        // given
        final c1 = V.int.lessThanOrEqualTo(5);
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate(6), false);
        expect(fin.evaluate(5), true);
        expect(fin.evaluate(4), true);
      });
      test('isGreaterThan', () {
        // given
        final c1 = V.int.greaterThan(5);
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate(5), false);
        expect(fin.evaluate(6), true);
      });
      test('isGreaterThanOrEqualTo', () {
        // given
        final c1 = V.int.greaterThanOrEqualTo(5);
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate(4), false);
        expect(fin.evaluate(5), true);
        expect(fin.evaluate(6), true);
      });
    });
    group('String', () {
      test('equalTo', () {
        // given
        final c1 = V.string.equalTo('x');
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate('x'), true);
        expect(fin.evaluate('y'), false);
      });
      test('notEqualTo', () {
        // given
        final c1 = V.string.notEqualTo('x');
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate('x'), false);
        expect(fin.evaluate('y'), true);
      });
      test('isNull', () {
        // given
        final c1 = V.string.isNull();
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate(null), true);
        expect(fin.evaluate('x'), false);
      });
      test('isNotNull', () {
        // given
        final c1 = V.string.isNotNull();
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate(null), false);
        expect(fin.evaluate('x'), true);
      });
      test('lengthEqualTo', () {
        // given
        final c1 = V.string.lengthEqualTo(2);
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate('xy'), true);
        expect(fin.evaluate('y'), false);
      });
      test('lengthNotEqualTo', () {
        // given
        final c1 = V.string.lengthNotEqualTo(2);
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate('xy'), false);
        expect(fin.evaluate('y'), true);
      });
      test('lengthLessThan', () {
        // given
        final c1 = V.string.lengthLessThan(2);
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate('xy'), false);
        expect(fin.evaluate('y'), true);
      });
      test('lengthLessThanOrEqualTo', () {
        // given
        final c1 = V.string.lengthLessThanOrEqualTo(2);
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate('xyz'), false);
        expect(fin.evaluate('xy'), true);
        expect(fin.evaluate('y'), true);
      });
      test('lengthGreaterThan', () {
        // given
        final c1 = V.string.lengthGreaterThan(2);
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate('xyz'), true);
        expect(fin.evaluate('xy'), false);
      });
      test('lengthGreaterThanOrEqualTo', () {
        // given
        final c1 = V.string.lengthGreaterThanOrEqualTo(2);
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate('xyz'), true);
        expect(fin.evaluate('xy'), true);
        expect(fin.evaluate('y'), false);
      });
      test('contains', () {
        // given
        final c1 = V.string.contains('x');
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate('xyz'), true);
        expect(fin.evaluate('zz'), false);
        expect(fin.evaluate('y'), false);
      });
      test('doesNotContain', () {
        // given
        final c1 = V.string.doesNotContain('x');
        // when
        final fin = c1.asFinal;
        // then
        expect(fin.evaluate('xyz'), false);
        expect(fin.evaluate('zz'), true);
        expect(fin.evaluate('y'), true);
      });
    });
  });
}

