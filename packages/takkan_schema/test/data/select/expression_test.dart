import 'package:takkan_schema/data/select/condition.dart';
import 'package:takkan_schema/data/select/expression.dart';
import 'package:takkan_schema/data/select/operation.dart';
import 'package:takkan_schema/schema/field/field.dart';
import 'package:test/test.dart';

import '../../fixtures.dart';

void main() {
  group('ExpressionConverter', () {
    const converter = ExpressionConverter();
    setUpAll(() {});

    tearDownAll(() {});

    setUp(() {});

    tearDown(() {});

    test('single statement equalTo', () {
      // given
      final field = Field<int>(property: 'a', validation: '== 5');
      final validationScript = field.validationScript;
      const queryExpression = 'a == 5';
      const expectedQ = Condition<int, dynamic>(
        fieldName: 'a',
        operator: Operator.equalTo,
        operand: 5,
        forQuery: true,
      );
      const expectedV = Condition<int, dynamic>(
        fieldName: 'a',
        operator: Operator.equalTo,
        operand: 5,
        forQuery: false,
      );

      // when
      final actualQ = converter.parseCondition<int>(
          expression: queryExpression, forQuery: true);
      final actualV = converter.parseCondition<int>(
          expression: validationScript, forQuery: false);
      // then

      expect(actualQ, ConditionMatcher(expectedQ));
      expect(actualV, ConditionMatcher(expectedV));
    });
    test('multi condition', () {
      // given
      final field = Field<int>(property: 'a', validation: '< 5  && > 3');
      final validationScript = field.validationScript;
      const queryExpression = 'a < 5 && a > 3';
      const expectedQ = [
        Condition<int, dynamic>(
          fieldName: 'a',
          operator: Operator.greaterThan,
          operand: 3,
          forQuery: true,
        ),
        Condition<int, dynamic>(
          fieldName: 'a',
          operator: Operator.lessThan,
          operand: 5,
          forQuery: true,
        )
      ];
      const expectedV = [
        Condition<int, dynamic>(
          fieldName: 'a',
          operator: Operator.greaterThan,
          operand: 3,
          forQuery: false,
        ),
        Condition<int, dynamic>(
          fieldName: 'a',
          operator: Operator.lessThan,
          operand: 5,
          forQuery: false,
        )
      ];

      // when
      final actualQ = converter.parseConditions<int>(
          script: queryExpression, forQuery: true);
      final actualV = converter.parseConditions<int>(
          script: validationScript, forQuery: false);
      // then

      expect(actualQ, containsAll(expectedQ.map(ConditionMatcher.new)));
      expect(actualV, containsAll(expectedV.map(ConditionMatcher.new)));
    });
  });
}
