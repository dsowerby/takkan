import 'package:takkan_schema/data/select/operation.dart';
import 'package:test/test.dart';

void main() {
  group('Operation', () {
    setUpAll(() {});

    tearDownAll(() {});

    setUp(() {});

    tearDown(() {});

    group('Operator', () {
      test('operatorScript contains all Operators', () {

        final operatorsInScript = operatorScript.keys.toSet();
        final allOperators = Operator.values.toSet();

        final missingOperators = allOperators.difference(operatorsInScript);

        expect(missingOperators.isEmpty, true,
            reason: 'Missing Operator values: $missingOperators');

      });
      test('reverseLookup contains only valid operatorScript strings', () {

        // Get flattened operatorScript strings
        final validStrings = operatorScript.values
            .expand((v) => v)
            .toSet();

        // Get invalid keys in reverseLookup
        final invalidKeys = scriptLookup.keys
            .toSet()
            .difference(validStrings);

        expect(invalidKeys, isEmpty,
            reason: 'Invalid reverseLookup keys: $invalidKeys');

      });
      test('reverseLookup should contain correct mappings', () {
        for (final entry in operatorScript.entries) {
          for (final string in entry.value) {
            expect(scriptLookup[string], entry.key,
                reason: 'Missing mapping for $string');
          }
        }

        expect(scriptLookup.length,
            operatorScript.values.map((v) => v.length).fold(0, (p, c) => p + c),
            reason: 'Total entries mismatch');
      });
      test('b4aSnippet contains all Operators', () {

        final operatorsInSnippets = b4aSnippet.keys.toSet();
        final allOperators = Operator.values.toSet();

        final missingOperators = allOperators.difference(operatorsInSnippets);

        expect(missingOperators.isEmpty, true,
            reason: 'Missing Operator values: $missingOperators');

      });
    });
  });
}
