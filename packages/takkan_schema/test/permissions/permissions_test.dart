import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:takkan_schema/schema/document/method.dart';
import 'package:takkan_schema/schema/document/permissions.dart';
import 'package:test/test.dart';

void main() {
  final basicMethods = MethodExtension.basicMethods;
  final readMethods = MethodExtension.readMethods;
  final writeMethods = MethodExtension.writeMethods;
  group('Permissions', () {
    setUpAll(() {});

    tearDownAll(() {});

    setUp(() {});

    tearDown(() {});

    test('default no permissions given', () {
      // given
      final Permissions permissions = Permissions();
      // when

      // then

      expect(permissions.requiresAuthentication(Method.get), true);
      expect(permissions.isPublic(Method.get), false);
      expect(permissions.permissions, isEmpty);
    });

    test('all set to authOnly', () {
      // given
      final Permissions permissions = Permissions(permissions: const {
        Method.all: [authOnly]
      });
      // when

      // then

      for (final method in basicMethods) {
        expect(permissions.permissions[method], [authOnly]);
        expect(permissions.requiresAuthentication(method), true);
        expect(permissions.isPublic(method), false);
        expect(permissions.hasPermission(method, true, {'admin'}), true);
        expect(permissions.hasPermission(method, true, {'author'}), true);
      }
    });
    test('read set to authOnly', () {
      // given
      final Permissions permissions = Permissions(permissions: const {
        Method.read: [authOnly]
      });
      // when

      // then

      for (final method in readMethods) {
        expect(permissions.permissions[method], [authOnly]);
        expect(permissions.requiresAuthentication(method), true);
        expect(permissions.isPublic(method), false);
        expect(permissions.hasPermission(method, true, {'admin'}), true);
        expect(permissions.hasPermission(method, true, {'author'}), true);
      }
      for (final method in writeMethods) {
        expect(permissions.permissions[method], null);
        expect(permissions.requiresAuthentication(method), true);
        expect(permissions.isPublic(method), false);
        expect(permissions.hasPermission(method, true, {'admin'}), false);
        expect(permissions.hasPermission(method, true, {'author'}), false);
      }
    });
    test('write set to authOnly', () {
      // given
      final Permissions permissions = Permissions(permissions: const {
        Method.write: [authOnly]
      });
      // when

      // then

      for (final method in readMethods) {
        expect(permissions.permissions[method], null);
        expect(permissions.requiresAuthentication(method), true);
        expect(permissions.isPublic(method), false);
        expect(permissions.hasPermission(method, true, {'admin'}), false);
        expect(permissions.hasPermission(method, true, {'author'}), false);
      }
      for (final method in writeMethods) {
        expect(permissions.permissions[method], [authOnly]);
        expect(permissions.requiresAuthentication(method), true);
        expect(permissions.isPublic(method), false);
        expect(permissions.hasPermission(method, true, {'admin'}), true);
        expect(permissions.hasPermission(method, true, {'author'}), true);
      }
    });

    test('role set, requiresAuthentication returns true', () {
      // given
      final Permissions permissions = Permissions(permissions: const {
        Method.all: ['admin']
      });
      // when

      // then

      for (final method in basicMethods) {
        expect(permissions.permissions[method], ['admin']);
        expect(permissions.requiresAuthentication(method), true);
        expect(permissions.isPublic(method), false);
        expect(permissions.hasPermission(method, true, {'admin'}), true);
        expect(permissions.hasPermission(method, true, {'author'}), false);
      }
    });
  });
  group('PermissionsDiff', () {
    test('adds new roles', () {
      // given
      final Permissions permissions = Permissions(permissions: const {
        Method.create: ['admin'],
        Method.get: [authOnly],
      });
      const PermissionsDiff diff = PermissionsDiff(addRoles: {
        Method.write: ['editor'],
        Method.find: ['admin'],
      });
      final expected = Permissions(permissions: const {
        Method.create: ['admin', 'editor'],
        Method.get: [authOnly],
        Method.delete: ['editor'],
        Method.update: ['editor'],
        Method.find: ['admin'],
        Method.addField: ['editor'],
      });
      // when
      final actual = diff.applyTo(permissions);
      // then
      expect(actual.roles(Method.addField),
          expected.roles(Method.addField));
      expect(
          actual.roles(Method.count), expected.roles(Method.count));
      expect(actual.roles(Method.create),
          expected.roles(Method.create));
      expect(actual.roles(Method.delete),
          expected.roles(Method.delete));
      expect(
          actual.roles(Method.find), expected.roles(Method.find));
      expect(actual.roles(Method.get), expected.roles(Method.get));
      expect(actual.roles(Method.update),
          expected.roles(Method.update));
    });
    test('remove roles', () {
      // given
      final base = Permissions(permissions: const {
        Method.create: ['admin', 'editor'],
        Method.get: [authOnly],
        Method.delete: ['editor'],
        Method.update: ['editor'],
        Method.find: ['admin'],
        Method.addField: ['editor'],
      });
      const diff = PermissionsDiff(removeRoles: {
        Method.delete: ['editor'],
        Method.create: ['editor'],
      }, addRoles: {
        Method.delete: ['admin']
      });
      final expected = Permissions(permissions: const {
        Method.create: ['admin'],
        Method.get: [authOnly],
        Method.delete: ['admin'],
        Method.update: ['editor'],
        Method.find: ['admin'],
        Method.addField: ['editor'],
      });
      // when
      final actual = diff.applyTo(base);
      // then
      for (final Method method in MethodExtension.basicMethods) {
        expect(actual.roles(method), expected.roles(method));
      }
    });
  });
  group('PermissionsFinal', () {
    test('Permissions transfer', () {
      // given
      final perms = IMap<MethodFinal, Set<String>>(const {
        MethodFinal.get: {authOnly},
        MethodFinal.delete: {'editor', 'owner'},
        MethodFinal.create: {publicAccess},
      });

      // when
      final permissions = PermissionsFinal(permissions: perms);
      // then
      expect(permissions.hasPermission(true, {}, MethodFinal.get), isTrue);
      expect(permissions.hasPermission(true, {'editor'}, MethodFinal.delete),
          isTrue,
          reason: 'editor has permission to delete');
      expect(permissions.hasPermission(true, {'owner'}, MethodFinal.delete),
          isTrue,
          reason: 'owner has permission to delete');
      expect(permissions.hasPermission(false, {'owner'}, MethodFinal.delete),
          isFalse,
          reason: 'unauthenticated does not have permission to delete');
      expect(permissions.hasPermission(false, {}, MethodFinal.create), isTrue,
          reason: 'anyone can create');
    });
  });
}
