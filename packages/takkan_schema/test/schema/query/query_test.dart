import 'package:takkan_schema/data/select/condition.dart';
import 'package:takkan_schema/data/select/operation.dart';
import 'package:takkan_schema/schema/document/document.dart';
import 'package:takkan_schema/schema/document/document_diff.dart';
import 'package:takkan_schema/schema/field/field.dart';
import 'package:takkan_schema/schema/query/query.dart';
import 'package:test/test.dart';

import '../../fixtures.dart';

void main() {
  group('Query', () {
    setUpAll(() {});

    tearDownAll(() {});

    setUp(() {});

    tearDown(() {});

    test('combine script and query', () {
      // given

      final document = Document(
        className: 'Person',
        fields: {
          'firstName': Field<String>(),
          'lastName': Field<String>(),
          'age': Field<int>(),
        },
        queries: {
          'adults': Query(
            constraints: [
              Q('age').int.equalTo(152),
              Q('lastName').string.equalTo('Hazel'),
            ],
            queryScript: "firstName == 'Jack'",
            returnSingle: true,
          ),
        },
      );

      const expectedConditionAge = Condition<int, int>(
        fieldName: 'age',
        forQuery: true,
        operator: Operator.equalTo,
        operand: 152,
      );
      // TODO:
      const expectedConditionFirstName = Condition<dynamic, dynamic>(
        fieldName: 'firstName',
        operator: Operator.equalTo,
        operand: 'Jack',
        forQuery: true,
      );
      const expectedConditionLastName = Condition<String, String>(
        fieldName: 'lastName',
        operator: Operator.equalTo,
        operand: 'Hazel',
        forQuery: true,
      );
      final expected = [
        expectedConditionAge,
        expectedConditionFirstName,
        expectedConditionLastName,
      ];

      // when
      final query = document.query('adults');
      query.buildConditions();
      final actual = query.conditions;
      // then

      expect(query.constraints.length, 2);
      expect(query.conditions.length, 3);
      expect(actual, containsAll(expected.map(ConditionMatcher.new)));
    });
  });
  group('QueryDiff', () {
    test('no changes', () {
      // given
      final amendAdults = QueryDiff(constraints: [
        Q('age').int.greaterThanOrEqualTo(18),
      ]);
      final doc = Document(
        className: 'doc',
        fields: {
          'firstName': Field<String>(),
          'lastName': Field<String>(),
          'age': Field<int>(),
        },
        queries: {
          'adults': Query(
            constraints: [
              Q('age').int.greaterThan(18),
            ],
          ),
          'Jack': Query(
            constraints: [
              Q('firstName').string.equalTo('Jack'),
            ],
          ),
        },
      );

      final teensQuery = Query(
        constraints: [
          Q('age').int.lessThan(18),
          Q('age').int.greaterThanOrEqualTo(13),
        ],
      );
      final documentDiff = DocumentDiff(
        addQueries: {'teens': teensQuery},
        removeQueries: ['Jack'],
        amendQueries: {'adults': amendAdults},
      );

      // when
      final actual = documentDiff.applyTo(doc);
      // then
      expect(actual.queries['Jack'], isNull);
      expect(actual.queries['teens'], teensQuery);
      expect(
          actual.queries['adults']?.constraints,
          [ConditionMatcher(
            Q('age').int.greaterThanOrEqualTo(18),
          )]);
    });
  });
}
