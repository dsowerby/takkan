import 'package:takkan_schema/data/select/condition.dart';
import 'package:takkan_schema/schema/document/document.dart';
import 'package:takkan_schema/schema/document/method.dart';
import 'package:takkan_schema/schema/field/field.dart';
import 'package:test/test.dart';

void main() {

  group('Document', () {
    setUpAll(() {});

    tearDownAll(() {});

    setUp(() {});

    tearDown(() {});

    group('Document', () {
      test('hasValidation', () {
        // given
        final f1 = Field<int>(conditions: [], required: false, readOnly: true);
        final f2 = Field<int>(
            conditions: [V.int.lessThanOrEqualTo(5)],
            required: true,
            readOnly: true);
        final doc1 = Document(className: 'doc1', fields: {});
        final doc2 = Document(className: 'doc2', fields: {'a': f1});
        final doc3 = Document(className: 'doc3', fields: {'a': f1,'b' : f2});
        final doc4 = Document(className: 'doc4', fields: {'b' : f2});
        // when

        // then
        expect(doc1.hasValidation, isFalse);
        expect(doc2.hasValidation, isFalse);
        expect(doc3.hasValidation, isTrue);
        expect(doc4.hasValidation, isTrue);

      });
      test('Document has default permissions', () {
        // given
        final Document document = Document(fields: const {}, className: '');
        // when

        // then
        expect(document.requiresAuthentication(Method.create), true);
        expect(document.requiresAuthentication(Method.find), true);
        expect(document.requiresAuthentication(Method.update), true);
        expect(document.requiresAuthentication(Method.delete), true);
        expect(document.requiresAuthentication(Method.get), true);
        expect(document.requiresAuthentication(Method.count), true);
        expect(document.requiresAuthentication(Method.addField), true);
      });
    });
    // group('DocumentDiff', () {
    //   test('diff makes no changes', () {
    //     // given
    //     final field1 = MockField();
    //     final doc = Document(fields: {'a': field1});
    //     const diff = DocumentDiff();
    //     // when
    //     final result = diff.applyTo(doc);
    //     // then
    //     expect(result, equals(doc));
    //   });
    //   test('diff changes fields', () {
    //     // given
    //     final fieldA = MockField();
    //     final fieldB = MockField();
    //     final fieldB1 = MockField();
    //     final fieldC = MockField();
    //     final fieldDiff = MockFieldDiff();
    //     when(() => fieldDiff.applyTo(fieldB)).thenReturn(fieldB1);
    //     final doc = Document(fields: {'a': fieldA, 'b': fieldB});
    //     final diff = DocumentDiff(
    //       addFields: {'c': fieldC},
    //       amendFields: {'b': fieldDiff},
    //       removeFields: ['a'],
    //     );
    //     // when
    //     final Document result = diff.applyTo(doc);
    //     // then
    //     expect(result.fields.containsValue(fieldA), isFalse);
    //     expect(result.fields.containsValue(fieldB), isFalse);
    //     expect(result.fields.containsValue(fieldB1), isTrue);
    //     expect(result.fields.containsValue(fieldC), isTrue);
    //   });
    // });
  });
}
