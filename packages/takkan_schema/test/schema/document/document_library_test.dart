import 'package:takkan_common/common/data.dart';
import 'package:takkan_common/common/version.dart';
import 'package:takkan_schema/schema/document/document.dart';
import 'package:takkan_schema/schema/document/document_library.dart';
import 'package:test/test.dart';


void main() {
  group('Document library', () {
    const appVersion = Version(versionIndex: 3);
    const datasource1 = Datasource(name: 'main', type: DatasourceType.back4app);
    const datasource2 = Datasource(name: 'other', type: DatasourceType.REST);
    const instance1 =
        DatasourceInstance(datasource: datasource1, instance: 'prod');
    const instance2 =
        DatasourceInstance(datasource: datasource2, instance: 'prod');
    final document1 =
        Document(className: 'doc1', fields: {});
    final document11 =
        Document(className: 'doc11', fields: {});
    final document2 =
        Document(className: 'doc2', fields: {});
    late DocumentLibrary lib;

    setUpAll(() {});

    tearDownAll(() {});

    setUp(() {
      lib = DocumentLibrary();
    });

    tearDown(() {});

    test('putting docs', () {
      // given

      // when
      lib.addDocument(document1,instance1);
      lib.addDocument(document2,instance2);
      // then
      expect(lib.document(instance1, 'doc1'), document1);
      expect(lib.document(instance2, 'doc2'), document2);
    });
    test('instance matches only on singleton', () {
      // given
      // ignore: prefer_const_constructors
      final datasource = Datasource(name: 'main', type: DatasourceType.back4app);
      // when

      // then
      expect(datasource.asFinal==datasource.asFinal, isFalse);
    });
    test('extract AppSchema', () {
      // given
      lib.addDocument(document1, instance1);
      lib.addDocument(document11,instance1);
      lib.addDocument(document2,instance2);
      // // when
      final appSchema = lib.toAppSchema(appVersion: appVersion);
      // // then
      final finalInstance1 = lib.instanceMap[instance1]!;
      final finalInstance2 = lib.instanceMap[instance2]!;
      final schema1 = appSchema.instanceSchemas[finalInstance1]!;
      final schema2 = appSchema.instanceSchemas[finalInstance2]!;
      expect(schema1.documents['doc1']?.datasourceInstance, finalInstance1);
      expect(schema1.documents['doc11']?.datasourceInstance, finalInstance1);
      expect(schema2.documents['doc2']?.datasourceInstance, finalInstance2);
      expect(appSchema.appVersion, appVersion);
    });
  });
}
