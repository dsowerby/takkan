import 'package:takkan_schema/data/select/condition.dart';
import 'package:takkan_schema/schema/field/field.dart';
import 'package:test/test.dart';

void main() {
  group('Field', () {
    setUpAll(() {});

    tearDownAll(() {});

    setUp(() {});

    tearDown(() {});

    // group('apply diff', () {
    //   test('remove default value', () {
    //     // given
    //     final field = Field<String>(defaultValue: 'x');
    //     final diff = FieldDiff<String>(removeDefaultValue: true);
    //     // when
    //     final result = diff.applyTo(field);
    //     // then
    //     expect(result.defaultValue, isNull);
    //   });
    //   test('replace default value', () {
    //     // given
    //     final field = Field<String>(defaultValue: 'x');
    //     final diff = FieldDiff<String>(defaultValue: 'y');
    //     // when
    //     final result = diff.applyTo(field);
    //     // then
    //     expect(result.defaultValue, 'y');
    //   });
    //   test('No change diff', () {
    //     // given
    //     final field = Field<String>(
    //       defaultValue: 'x',
    //       validation: 'x isLongerThan 5',
    //       conditions: [V.string.lengthGreaterThan(5)],
    //       required: true,
    //       isReadOnly: IsReadOnly.yes,
    //       index: Index.ascending,
    //     );
    //     final diff = FieldDiff<String>();
    //     // when
    //     final result = diff.applyTo(field);
    //     // then
    //     expect(result, field);
    //   });
    //   test('Diff changes all', () {
    //     // given
    //     final field = Field<String>(
    //       defaultValue: 'x',
    //       validation: 'isLongerThan 5',
    //       conditions: [V.string.lengthGreaterThan(5)],
    //       required: true,
    //       isReadOnly: IsReadOnly.yes,
    //       index: Index.ascending,
    //     );
    //     final diff = FieldDiff<String>(
    //       defaultValue: 'y',
    //       validation: 'isLongerThan 3',
    //       constraints: [
    //         V.string.lengthGreaterThan(3),
    //         V.string.lengthLessThan(5)
    //       ],
    //       required: false,
    //       isReadOnly: IsReadOnly.inherited,
    //       index: Index.descending,
    //     );
    //     // when
    //     final result = diff.applyTo(field);
    //     // then
    //     expect(result.defaultValue, 'y');
    //     expect(result.validation, 'isLongerThan 3');
    //     expect(result.conditions, [
    //       V.string.lengthGreaterThan(3),
    //       V.string.lengthLessThan(5),
    //     ]);
    //     expect(result.required, false);
    //     expect(result.isReadOnly, IsReadOnly.inherited);
    //     expect(result.index, Index.descending);
    //   });
    //   test('hasValidation', () {
    //     // given
    //     final schema = Schema(documents: {
    //       'Person': Document(fields: {
    //         'firstName': Field<String>(),
    //         'familyName': Field<String>(validation: 'length> 5'),
    //         'age': Field<int>(conditions: [V.int.greaterThan(0)]),
    //         'height': Field<int>(required: true),
    //       })
    //     });
    //     // when
    //     schema.init(schemaName: 'test');
    //     final firstNameField = schema.documents['Person']!.fields['firstName']!;
    //     final lastNameField = schema.documents['Person']!.fields['familyName']!;
    //     final ageField = schema.documents['Person']!.fields['age']!;
    //     final heightField = schema.documents['Person']!.fields['height']!;
    //     // then
    //     expect(firstNameField.hasValidation, false);
    //     expect(lastNameField.hasValidation, true);
    //     expect(ageField.hasValidation, true);
    //     expect(heightField.hasValidation, true);
    //   });
    // });
    test('Field', () {
      // given
      final f1 = Field(required: true);
      final f2 = Field(required: false);
      final f3 =
          Field(required: false, conditions: [V.string.lengthGreaterThan(5)]);
      // when

      // then
      expect(f1.hasValidation, true);
      expect(f2.hasValidation, false);
      expect(f3.hasValidation, true);
    });
    test('validationScript', () {
      // given
      final field = Field<int>(property: 'a', validation: '< 5 && > 3');
      // when
      final result = field.validationScript;
      // then
      expect(result, 'a < 5 && a > 3');
    });
  });
}
