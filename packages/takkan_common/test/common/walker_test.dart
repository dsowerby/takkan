import 'dart:math';

import 'package:takkan_common/common/walker.dart';
import 'package:test/test.dart';

void main() {
  group('Walker', () {
    setUpAll(() {});

    tearDownAll(() {});

    setUp(() {});

    tearDown(() {});

    test('Empty subElements', () {
      // given
      final target = D();
      final visitor = SimpleVisitor();
      const params = EmptyWalkerParams();
      // when
      target.walk(EmptyWalkable(), [visitor], params: params);
      // then
      expect(visitor.visits.length, 1);
      expect(visitor.visits, containsAll([target]));
    });
    test('One level of children', () {
      // given
      final b = B(id: 'b', cs: []);
      final target = A(id: 'a', bs: [b]);
      final visitor = SimpleVisitor();
      const params = EmptyWalkerParams();
      // when
      target.walk(EmptyWalkable(), [visitor], params: params);
      // then
      expect(visitor.visits.length, 2);
      expect(visitor.visits, containsAll([target, b]));
    });
    test('Two levels of children', () {
      // given
      final c = C(id: 'c');
      final b = B(id: 'b', cs: [c]);
      final target = A(id: 'a', bs: [b]);
      final visitor = SimpleVisitor();
      const params = EmptyWalkerParams();
      // when
      target.walk(EmptyWalkable(), [visitor], params: params);
      // then
      expect(visitor.visits.length, 3);
      expect(visitor.visits, containsAll([target, b, c]));
    });
    test('Two levels of children, mixed targets', () {
      // given
      final c = C(id: 'c');
      final w1 = Wiggly();
      final w2 = Wiggly();
      final b = B(id: 'b', cs: [c], wigglies: [w1, w2]);
      final target = A(id: 'a', bs: [b]);
      final visitor = SimpleVisitor();
      const params = EmptyWalkerParams();
      // when
      target.walk(EmptyWalkable(), [visitor], params: params);
      // then
      expect(visitor.visits.length, 5);
      expect(visitor.visits, containsAll([target, b, c, w1, w2]));
    });
    test('modified params', () {
      // given
      final c = C(id: 'c');
      final w1 = Wiggly();
      final w2 = Wiggly();
      final b = B(id: 'b', cs: [c], wigglies: [w1, w2]);
      final target = A(id: 'a', bs: [b]);
      final visitor = ModifyingVisitor();
      final params = CommonParams();
      // when
      target.walk(EmptyWalkable(), [visitor], params: params);
      // then
      expect(visitor.visits.length, 5);
      expect(visitor.visits, containsAll([target, b, c, w1, w2]));
      expect(target.debugId, 'a');
      expect(b.debugId, 'a:b');
      expect(c.debugId, 'a:b:c');
    });
    test('type collector', () {
      // given
      final c = C(id: 'c');
      final w1 = Wiggly();
      final w2 = Wiggly();
      final b = B(id: 'b', cs: [c], wigglies: [w1, w2]);
      final target = A(id: 'a', bs: [b]);
      final visitor = TypeCollectingVisitor<Wiggly>();
      final params = CommonParams();
      // when
      target.walk(EmptyWalkable(), [visitor], params: params);
      // then
      expect(visitor.collected.length, 2);
      expect(visitor.collected, containsAll([w1, w2]));
    });
    test('parent child collector', () {
      // given
      final c = C(id: 'c');
      final b = B(id: 'b', cs: [c]);
      final target = A(id: 'a', bs: [b]);
      final visitor = ParentChildVisitor();
      const params = EmptyWalkerParams();
      // when
      target.walk(EmptyWalkable(), [visitor], params: params);
      // then
      expect(visitor.visits.length, 3);
      expect(visitor.visits[c], b);
      expect(visitor.visits[b], target);
    });
  });
}

abstract class Common {
  late String debugId;
  final String id;

  Common({
    required this.id,
  });
}

class A extends Common with Walkable {
  final List<B> bs;

  @override
  List<Object?> get subElements => [bs];

  A({
    required this.bs,
    required super.id,
  });
}

class B extends Common with Walkable {
  final List<C> cs;
  final List<Wiggly> wigglies;

  @override
  List<Object?> get subElements => [cs, null, wigglies];

  B({
    required this.cs,
    this.wigglies = const [],
    required super.id,
  });
}

class C extends Common with Walkable {
  @override
  List<Object?> get subElements => [];

  C({required super.id});
}

class D with Walkable {
  @override
  List<Object?> get subElements => [];
}

/// Not Walkable
class Wiggly {}

class SimpleVisitor implements WalkVisitor {
  final visits = <Object>[];

  @override
  void visit(Walkable parent, Object entry,
      {WalkerParams params = const EmptyWalkerParams()}) {
    visits.add(entry);
  }
}

class ParentChildVisitor implements WalkVisitor {
  final visits = < Object,Walkable>{};

  @override
  void visit(Walkable parent, Object entry,
      {WalkerParams params = const EmptyWalkerParams()}) {
    visits[entry] = parent;
  }
}

class ModifyingVisitor implements WalkVisitor {
  final visits = <Object>[];

  @override
  void visit(Walkable parent, Object entry,
      {WalkerParams params = const EmptyWalkerParams()}) {
    visits.add(entry);
    final CommonParams p = params as CommonParams;
    if (entry is Common) {
      entry.debugId = p.debugId == null ? entry.id : '${p.debugId}:${entry.id}';
      params.debugId = entry.debugId;
    }
  }
}

class CommonParams implements WalkerParams {
  String? id;
  String? debugId;

  CommonParams({
    this.id,
    this.debugId,
  });
}
