// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DatasourceInstance _$DatastoreInstanceFromJson(Map<String, dynamic> json) =>
    DatasourceInstance(
      datasource: Datasource.fromJson(json['datastore'] as Map<String, dynamic>),
      instance: json['instance'] as String,
    );

Map<String, dynamic> _$DatastoreInstanceToJson(DatasourceInstance instance) =>
    <String, dynamic>{
      'datastore': instance.datasource.toJson(),
      'instance': instance.instance,
    };

Datasource _$DatastoreFromJson(Map<String, dynamic> json) => Datasource(
      name: json['name'] as String,
      type: $enumDecode(_$DatastoreTypeEnumMap, json['type']),
    );

Map<String, dynamic> _$DatastoreToJson(Datasource instance) => <String, dynamic>{
      'name': instance.name,
      'type': _$DatastoreTypeEnumMap[instance.type]!,
    };

const _$DatastoreTypeEnumMap = {
  DatasourceType.back4app: 'back4app',
  DatasourceType.REST: 'REST',
};
