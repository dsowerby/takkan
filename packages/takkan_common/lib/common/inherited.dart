import 'package:takkan_common/common/serial.dart';

import 'log.dart';

class InheritanceException implements Exception {
  const InheritanceException();

  String errMsg() => 'Calls to this class should be made only after it has been initialised with a call to copyInherited';
}

/// Just does a null check on a property and throws an [InheritedException] if
/// null
T checkInherited<T> (Object source, T? o) {
  if (o == null) {
    logType(T).e('null value passed to checkInherited in ${source.runtimeType}');
    throw InheritanceException();
  }
  return o;
}





/// To simplify declaration by the developer, many properties are inherited
/// through the tree structure of the [Composer].  The [HasFinal] interface is used
/// to prompt the IDE to ensure that the implementation provides the [inherited]
/// getter, along with the common requirement to [applyInheritedValues] from
/// the [PARENT].
abstract class HasFinal < FINAL extends Final> extends Serializable{
  FINAL get asFinal;
}


/// Used purely for type safety, implemented by all xxxxFinal classes,
/// implementations represent the last step in ensuring that the tree
/// is immutable and const.
abstract class Final{

}
