/// [WalkVisitor],[Walkable] and [WalkParams] all play their part
/// in walking a tree structure. See [Walkable] for a detailed description
abstract class WalkVisitor<PARAMS extends WalkerParams> {
  void visit(Walkable parent, Object entry, {PARAMS params});
}

/// [WalkVisitor],[Walkable] and [WalkParams] all play their part
/// in walking a tree structure.  In short, the called implementation and all its
/// [subElements] are passed to all [visitors].  Any [subElements] which also
/// implement [Walkable] have their [walk] method invoked, thus cascading the call.
/// The tree is traversed depth first, as often the requirement is to pass
/// information from parent to child.
///
/// The [parent] is always passed to its children. For the first call, it may be
/// desirable or even necessary to declare the the first call with no parent using
/// [EmptyWalkable].
///
/// [Walkable.subElements] are considered to be children of its implementing class,
/// and are passed in turn to the [WalkVisitor.visit].  Note that [Walkable.subElements]
/// do not have to be [Walkable] themselves, but if they are not, the walk will not
/// cascaded further - they will still be passed to the [visitors], but as they
/// do not implement [Walkable] there is nothing for them to cascade to.
///
/// If a map is declared in [subElements], any values declaring the [LateNamed]
/// interface will have the associated map key passed as the value name, before
/// being passed to the [visitors].
///
/// **Note:** A [WalkVisitor] may modify [WalkParams].  Take care therefore not
/// to introduce unexpected side effects, especially when passing multiple visitors
abstract mixin class Walkable {
  /// A depth first iteration of a [Walkable] tree.
  ///
  /// - [childIndex] is incremented for each of the [subElements]
  /// - [walkableChildIndex] is incremented only for [Walkable] [subElements]
  /// - [parentKey] should be passed when this is held by the parent, as a value
  /// in a Map, with an expectation of the map key being used to name the value.
  /// Usually this is achieved using the [LateNamed] interface.
  void walk(
    Walkable parent,
    List<WalkVisitor> visitors, {
    int childIndex = 0,
    int walkableChildIndex = 0,
    WalkerParams params = const EmptyWalkerParams(),
  }) {
    // Pass this first
    for (final WalkVisitor visitor in visitors) {
      visitor.visit(parent, this, params: params);
    }
    // Pass all children to all visitors, incrementing childIndex
    // but first expand any collections defined in subElements
    // Mps are checked for values with [LateNamed] values
    final expandedSubElements = <Object>[];
    for (final Object? element in subElements) {
      if (element != null) {
        if (element is Map) {
          final Map map = element;
          map.forEach((key, value) {
            if (value is LateNamed) {
              value.lateName(key);
            }
            expandedSubElements.add(value);
          });
        }
        else if (element is Iterable<dynamic>) {
          for (final Object? e in element) {
            if (e != null) {
              expandedSubElements.add(e);
            }
          }
        } else {
          expandedSubElements.add(element);
        }
      }
    }

    for (final Object? subElement in expandedSubElements) {
      if (subElement != null) {
        int cIndex = 0;
        int wcIndex = 0;
        for (final WalkVisitor visitor in visitors) {
          // subElement will pass itself to visitors
          if (subElement is Walkable) {
            subElement.walk(
              this,
              visitors,
              params: params,
              childIndex: cIndex,
              walkableChildIndex: wcIndex,
            );
            wcIndex++;
          } else {
            visitor.visit(this, subElement, params: params);
          }
          cIndex++;
        }
      }
    }
  }

  /// Those properties which should be treated as 'children' within the tree
  /// structure. DO NOT expand maps here, for example subElements=> [...children]
  /// where the map key is used to name the value.
  ///
  /// All collections are expanded are by the tree walk.
  ///
  /// [subElements] do not need to implement [Walkable] but if they do not, the
  /// tree walk will not progress beyond them.
  List<Object?> get subElements;
}

/// Purely for type safety
abstract class WalkerParams {
  const WalkerParams();
}

/// Just saves handling nulls
class EmptyWalkerParams implements WalkerParams {
  const EmptyWalkerParams();
}

/// Just saves handling nulls
class EmptyWalkable with Walkable {
  @override
  List<Object?> get subElements => const [];
}

/// Convenience class to collect
class TypeCollectingVisitor<T> extends WalkVisitor {
  final collected = <T>[];

  @override
  void visit(Walkable parent, Object entry,
      {WalkerParams params = const EmptyWalkerParams()}) {
    if (entry is T) {
      collected.add(entry as T);
    }
  }
}

/// Some classes receive their name from a Map key, to make it easier to declare
/// them.  This interface helps identify them and set their name as part of the
/// Composer init process.
abstract class LateNamed {
  void lateName(String name);
}
