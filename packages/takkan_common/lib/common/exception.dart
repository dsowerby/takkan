class TakkanException implements Exception {
  const TakkanException(this.msg);
  final String msg;

  String errMsg() => msg;
}

class SchemaException implements Exception {
  const SchemaException(this.msg);
  final String msg;

  String errMsg() => msg;
}

class InheritanceException implements Exception {
  const InheritanceException();

  String errMsg() => 'Calls to this class should be made only after it has been initialised with a call to copyInherited';
}
