import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:takkan_common/common/serial.dart';

import 'inherited.dart';

part 'data.g.dart';

/// Defines where to read / write data from.  It can be a Back4App datastore or
/// a REST API. and there may be multiple datastores within an application.
///
/// It relates to the the *takkan.json* file used to configure the application.
///
/// [datasource] relates to the first level within *takkan.json*
/// [instance] relates to the second level within *takkan.json*
///
/// For example, [datasource] maybe called 'main' for your main application database,
/// and [instance] could be something like 'dev' or 'prod'.
///
/// Note that [DatasourceInstance] defines [==] and [hashCode], enabling compare on
/// property values, while [DatasourceInstanceFinal] does not, matching only on instance
/// equality.  This is deliberate, as the aim is to have single instances for
/// all [Final] values.  See [DocumentLibrary] for how this is used.
@JsonSerializable(explicitToJson: true)
class DatasourceInstance extends Equatable
    implements Serializable, HasFinal<DatasourceInstanceFinal> {
  const DatasourceInstance({
    required this.datasource,
    required this.instance,
  });

  factory DatasourceInstance.fromJson(Map<String, dynamic> json) =>
      _$DatastoreInstanceFromJson(json);

  final Datasource datasource;
  final String instance;

  @JsonKey(includeToJson: false, includeFromJson: false)
  @override
  List<Object?> get props => [datasource, instance];

  @override
  Map<String, dynamic> toJson() => _$DatastoreInstanceToJson(this);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    if (other.runtimeType != runtimeType) return false;
    return other is DatasourceInstance &&
        other.datasource == datasource &&
        other.instance == instance;
  }

  @override
  int get hashCode => Object.hash(datasource, instance);

  @override
  DatasourceInstanceFinal get asFinal =>
      DatasourceInstanceFinal(datasource: datasource.asFinal, instance: instance);
}

class DatasourceInstanceFinal implements Final {
  final DatasourceFinal datasource;
  final String instance;

  const DatasourceInstanceFinal({
    required this.datasource,
    required this.instance,
  });
}

/// Defines where to read / write data from.  It can be a Back4App datastore or
/// a REST API. and there may be multiple datastores within an application.
///
/// It relates to the first level of the *takkan.json* file used to configure
/// the application.
///
/// Note that [Datasource] defines [==] and [hashCode], enabling compare on
/// property values, while [DatasourceFinal] does not, matching only on instance
/// equality.  This is deliberate, as the aim is to have single instances for
/// all [Final] values.  See [DocumentLibrary] for how this is used.
@JsonSerializable(explicitToJson: true)
class Datasource implements Serializable, HasFinal<DatasourceFinal> {
  final String name;

  final DatasourceType type;

  factory Datasource.fromJson(Map<String, dynamic> json) =>
      _$DatastoreFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$DatastoreToJson(this);

  const Datasource({
    required this.name,
    required this.type,
  });

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    if (other.runtimeType != runtimeType) return false;
    return other is Datasource && other.name == name && other.type == type;
  }

  @override
  int get hashCode => Object.hash(name, type);

  @override
  get asFinal => DatasourceFinal(name: name, type: type);
}

enum DatasourceType {
  back4app,
  REST,
}

class DatasourceFinal implements Final {
  final String name;
  final DatasourceType type;

  const DatasourceFinal({
    required this.name,
    required this.type,
  });
}
