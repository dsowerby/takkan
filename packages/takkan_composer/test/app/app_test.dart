import 'package:takkan_common/common/data.dart';
import 'package:takkan_common/common/version.dart';
import 'package:takkan_composer/composer/composer.dart';
import 'package:takkan_composer/content/control_edit.dart';
import 'package:takkan_composer/feature/feature.dart';
import 'package:takkan_composer/page/page.dart';
import 'package:takkan_composer/panel/panel.dart';
import 'package:takkan_composer/part/part.dart';
import 'package:takkan_schema/data/select/condition.dart';
import 'package:takkan_schema/data/select/condition_final.dart';
import 'package:takkan_schema/data/select/operation.dart';
import 'package:takkan_schema/schema/document/document.dart';
import 'package:takkan_schema/schema/field/field.dart';
import 'package:test/test.dart';

void main() {
  group('App structure', () {
    setUpAll(() {});

    tearDownAll(() {});

    setUp(() {});

    tearDown(() {});

    test('final output', () {
      final platformDocument = Document(className: 'Platform', fields: {});
      final weatherDocument = Document(className: 'Weather', fields: {});
      final addressDocument = Document(className: 'Address', fields: {
        'firstLine': Field<String>(
          conditions: [
            V.string.isNotNull(),
          ],
        ),
        'secondLine': Field<String>(
          conditions: [
            V.string.lengthGreaterThan(3),
          ],
        ),
      });
      final dataInstance1 = DatasourceInstance(
          datasource: Datasource(name: 'main', type: DatasourceType.back4app),
          instance: 'prod');
      final dataInstance2 = DatasourceInstance(
          datasource: Datasource(name: 'other', type: DatasourceType.back4app),
          instance: 'prod');
      // given
      final app = Composer(
        name: 'app',
        version: Version(versionIndex: 2),
        features: {
          'core': Feature(
            name: 'core',
            dataInstance: dataInstance1,
            pages: {
              'home': Page(
                content: {
                  'user': Part(
                    document: platformDocument,
                  ),
                  'address': Panel(
                    controlEdit: [ControlEdit.yes],
                    document: addressDocument,
                    content: {
                      'firstLine': Part(),
                      'secondLine': Part(),
                    },
                  ),
                },
              ),
              'about': Page(
                content: {
                  'temperature': Part(
                    dataInstance: dataInstance2,
                    document: weatherDocument,
                  ),
                },
              ),
            },
          )
        },
      );

      // when
      final fin = app.init();
      final dataInstance1fin = findFinalDataInstance(fin, dataInstance1);
      final dataInstance2fin = findFinalDataInstance(fin, dataInstance2);

      // then
      expect(fin, isA<ComposerFinal>());
      expect(fin.features.length, 1);

      final coreFeature = fin.features['core']!;
      expect(coreFeature, isA<FeatureFinal>());
      expect(coreFeature.name, 'core');
      expect(coreFeature.route, '/core');
      expect(coreFeature.pages.length, 2);

      final homePageFinal = coreFeature.pages[0];
      final aboutPageFinal = coreFeature.pages[1];

      expect(homePageFinal, isA<PageFinal>());
      expect(homePageFinal.property, 'home');
      expect(homePageFinal.route, '/core/home');
      expect(homePageFinal.content.length, 2);
      expect(homePageFinal.showEditControls, true);

      expect(aboutPageFinal, isA<PageFinal>());
      expect(aboutPageFinal.property, 'about');
      expect(aboutPageFinal.route, '/core/about');
      expect(aboutPageFinal.content.length, 1);
      expect(aboutPageFinal.showEditControls, true);

      final userPart = homePageFinal.content[0] as PartFinal;
      final addressPanel = homePageFinal.content[1] as PanelFinal;

      expect(userPart.property, 'user');
      expect(userPart.document, platformDocument);
      expect(userPart.route, '/core/home.user');
      expect(userPart.dataInstance, dataInstance1);
      expect(userPart.showEditControls, false);

      expect(addressPanel.property, 'address');
      expect(addressPanel.route, '/core/home.address');
      expect(addressPanel.showEditControls, true);

      final addressLine1 = addressPanel.content[0] as PartFinal;
      final addressLine2 = addressPanel.content[1] as PartFinal;

      expect(addressLine1.property, 'firstLine');
      expect(addressLine1.route, '/core/home.address.firstLine');
      expect(addressLine1.document, addressDocument);
      expect(addressLine1.dataInstance, dataInstance1);
      expect(addressLine1.showEditControls, true);

      expect(addressLine2.property, 'secondLine');
      expect(addressLine2.route, '/core/home.address.secondLine');
      expect(addressLine2.document, addressDocument);
      expect(addressLine2.dataInstance, dataInstance1);
      expect(addressLine2.showEditControls, true);

      // Routes
      expect(fin.routeMap['/core/home'], homePageFinal);

      // Schema
      expect(fin.schema.appVersion, Version(versionIndex: 2));
      expect(fin.schema.instanceSchemas.length, 2);
      expect(fin.schema.instanceSchemas[dataInstance1fin]?.documents.length, 2);
      expect(fin.schema.instanceSchemas[dataInstance2fin]?.documents.length, 1);

      final DocumentFinal addressDocumentFinal =
          fin.schema.instanceSchemas[dataInstance1fin]!.documents['Address']!;
      expect(addressDocumentFinal.className, addressDocument.className);
      expect(addressDocumentFinal.fields.length, addressDocument.fields.length);
      expect(addressDocumentFinal.fields['firstLine']!.conditions[0].operator,
          Operator.isNotNull);
      expect(addressDocumentFinal.fields['secondLine']!.conditions[0].operator,
          Operator.lengthGreaterThan);
    });
  });
}

/// The [DocumentLibrary] is discarded by now, as it it used by the [InitVisitor]
/// The only way we can identify the [DatasourceInstanceFinal] is by doing a
/// field compare with the [DatasourceInstance]
DatasourceInstanceFinal findFinalDataInstance(
    ComposerFinal fin, DatasourceInstance dataInstance1) {
  for (final instance in fin.schema.instanceSchemas.keys) {
    if (instance.datasource.name == dataInstance1.datasource.name &&
        instance.datasource.type == dataInstance1.datasource.type &&
        instance.instance == dataInstance1.instance) {
      return instance;
    }
  }
  throw UnsupportedError('Unable to identify the instance');
}
