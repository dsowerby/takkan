// ignore_for_file: avoid_implementing_value_types

import 'package:mocktail/mocktail.dart';
import 'package:takkan_common/common/exception.dart';
import 'package:takkan_schema/schema/document/document.dart';
import 'package:takkan_schema/schema/document/permissions.dart';
import 'package:takkan_schema/schema/field/field.dart';
import 'package:takkan_schema/schema/field/field_diff.dart';
import 'package:test/test.dart';



Matcher throwsTakkanException = throwsA(isA<TakkanException>());

// ignore: strict_raw_type
class MockField extends Mock implements Field {}
class MockFieldDiff extends Mock implements FieldDiff {}
class MockDocument extends Mock implements Document {}
class MockPermissions extends Mock implements Permissions {}

