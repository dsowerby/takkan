import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:takkan_common/common/inherited.dart';
import 'package:takkan_common/common/serial.dart';
import 'package:takkan_common/common/walker.dart';
import 'package:takkan_composer/page/page.dart';
import 'package:takkan_composer/page/page_child.dart';
import 'package:takkan_composer/panel/panel_layout.dart';
import 'package:takkan_schema/schema/document/document.dart';

import '../content/control_edit.dart';
import '../part/part.dart';
import '../common/properties.dart';

part 'panel.g.dart';

/// Panel contains one or more [Part]s
@JsonSerializable(explicitToJson: true)
class Panel extends PresentationProperties
    implements PanelChild, PageChild, HasFinal<PanelFinal> ,LateNamed{
  final Map<String, Part> content;
  @PanelLayoutJsonConverter()
  final PanelLayout layout;

  Panel({
    required this.content,
    super.property,
    super.document,
    this.layout = const DefaultPanelLayout(),
    super.dataInstance,
    super.controlEdit,
  });

  factory Panel.fromJson(Map<String, dynamic> json) => _$PanelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$PanelToJson(this);


  @override
  PanelFinal get asFinal => PanelFinal(
      showEditControls: showEditControls,
      route: route,
      property: property,
      content: IList(content.values.map((e) => e.asFinal)));



  @override
  String get routeSeparator => '.';

  /// [showEditControls] is set to show edit controls or not, depending on
  /// the values in [controlEdit]. [controlEdit] may have been inherited or set
  /// explicitly in this instance.  Cascading inherited values must be done
  /// before this is calculated
  @override
  bool convertControlEdit(PresentationProperties parent) {
    return controlEdit.contains(ControlEdit.allPanels) ||
        controlEdit.contains(ControlEdit.yes) ||
        controlEdit.contains(ControlEdit.firstLevelPanels) && (parent is Page);
  }

  @override
  void lateName(String name) => applyName(name);

  @override
  List<Object?> get subElements => [content];
}

class PanelFinal implements Final, PanelChildFinal, PageChildFinal {
  final String route;
  final IList<PanelChildFinal> content;
  final String property;
  final bool showEditControls;

  const PanelFinal({
    required this.route,
    required this.content,
    required this.property,
    required this.showEditControls,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PanelFinal &&
          runtimeType == other.runtimeType &&
          route == other.route &&
          content == other.content;

  @override
  int get hashCode => Object.hash(route, content);
}

/// Type safety for items which can be held as content within a [Panel], namely
/// [Panel] or [Part].  See also [PanelChildFinal]
abstract class PanelChild extends Serializable {
}

/// Implementations which can be held as content within a [PanelFinal], namely
/// [PanelFinal] or [PartFinal].  See also [PanelChild]
abstract class PanelChildFinal extends Final {}
