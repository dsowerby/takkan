// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'panel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Panel _$PanelFromJson(Map<String, dynamic> json) => Panel(
      content: (json['content'] as Map<String, dynamic>).map(
        (k, e) => MapEntry(k, Part.fromJson(e as Map<String, dynamic>)),
      ),
      property: json['property'] as String?,
      layout: json['layout'] == null
          ? const DefaultPanelLayout()
          : const PanelLayoutJsonConverter()
              .fromJson(json['layout'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PanelToJson(Panel instance) => <String, dynamic>{
      'property': instance.property,
      'content': instance.content.map((k, e) => MapEntry(k, e.toJson())),
      'layout': const PanelLayoutJsonConverter().toJson(instance.layout),
    };
