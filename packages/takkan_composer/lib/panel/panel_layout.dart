import 'package:json_annotation/json_annotation.dart';
import 'package:takkan_common/common/constants.dart';
import 'package:takkan_common/common/log.dart';
import 'package:takkan_common/common/serial.dart';

part 'panel_layout.g.dart';
abstract class PanelLayout extends Serializable {}

@JsonSerializable(explicitToJson: true)
class DefaultPanelLayout implements PanelLayout {
  const DefaultPanelLayout();

  factory DefaultPanelLayout.fromJson(Map<String, dynamic> json) =>
      _$DefaultPanelLayoutFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$DefaultPanelLayoutToJson(this);
}

class PanelLayoutJsonConverter
    extends JsonConverter<PanelLayout, Map<String, dynamic>> {
  const PanelLayoutJsonConverter();

  @override
  PanelLayout fromJson(Map<String, dynamic> json) {
    final elementType = json[jsonClassKey];
    switch (elementType) {
      case 'DefaultPanelLayout':
        return DefaultPanelLayout.fromJson(json);

      default:
        final msg = 'PanelLayout type $elementType not recognised';
        logType(runtimeType).e(msg);
        throw Exception(msg);
    }
  }

  @override
  Map<String, dynamic> toJson(PanelLayout object) {
    final type = object.runtimeType;
    final outputMap = <String, dynamic>{};
    outputMap[jsonClassKey] = type.toString();
    outputMap.addAll(object.toJson());
    return outputMap;
  }
}

