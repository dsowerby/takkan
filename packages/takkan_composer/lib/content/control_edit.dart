import 'package:json_annotation/json_annotation.dart';
import 'package:takkan_schema/schema/common/schema_element.dart';

import '../composer/composer_element.dart';
import '../panel/panel.dart';
import '../page/page.dart';
import '../part/part.dart';
import 'help.dart';


/// Describes the level in the [Composer] hierarchy that offers the user the
/// option to edit the data displayed.  It can be set anywhere in the hierarchy,
/// and cascades to all lower levels, unless changed by a lower level.
///
/// The following enables edit as described, and can be combined using multiple
/// instance of this enum.
///
/// [yes] for this instance (but remember this will be cascaded unless
/// overwritten)
/// [no] er ... no
/// [firstLevelPanels] only the first level of Panels within a [Page]
/// [firstLevelPages] only the first level of Pages, that is not including sub-pages
/// [allPages]
/// [allParts]
/// [allPanels]
enum ControlEdit {
  yes,
  allPages,
  allPanels,
  allParts,
  firstLevelPanels,
  firstLevelPages,
  no,
}
