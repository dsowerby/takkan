import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:takkan_common/common/data.dart';
import 'package:takkan_common/common/inherited.dart';
import 'package:takkan_common/common/walker.dart';
import 'package:takkan_schema/schema/document/document.dart';

import '../composer/composer.dart';
import '../composer/composer_element.dart';
import '../content/control_edit.dart';
import '../page/page.dart';
import '../page/page_child.dart';
import '../panel/panel.dart';
import '../common/properties.dart';

part 'part.g.dart';

/// Represents a single data item (for example from a database column).
/// Holds presentation elements for both read and edit states, unless it is
/// read only
///
/// Contained within either a [Panel] or a [Page], a [Part], presents the data
/// for a single field.
///
/// A [Part] usually presents two different widgets, one for read mode and one
/// for edit mode, although if the [Part] is read only, it will usually only
/// create the read widget.
///
/// More sophisticated Parts may present further widgets depending on configuration.
///
/// This is to avoid having a nullable [document] and [dataInstance] in the
/// [Composer],[Feature], [Panel], and [Page] classes.
/// [T] is the data type as held by the database. Type conversion is handled
/// automatically within the *takkan_client* package
///
/// [staticData] - used either on its own or combined with dynamic data. For example 'Welcome David' could be constructed from [staticData] of 'Welcome ' and 'David' from the logged in user's name. See [Localisation](https://www.takkanblog.co.uk/user-guide/takkan-model.html#localisation).
/// Takes precedence over [property].  For a full description of how to use static and dynamic data together, see:
/// [property] - the property to look up in order to get the data value.  This connects a data binding to the [EditState] immediately above the [Part] Widget associated with this configuration.
/// If [property] is null, [isStatic] will return true. For a full description of how to use static and dynamic data together, see:
/// [caption] - the text to display as a caption.  See [Localisation](https://www.takkanblog.co.uk/user-guide/takkan-model.html#localisation)
/// [readOnly] - if true, this part is always in read only mode, regardless of any other edit state settings.  If false, the [Part] will respond to the current edit state of the [EditState] immediately above it.
/// [help] - if non-null a small help icon button will popup when clicked. See [Localisation](https://www.takkanblog.co.uk/user-guide/takkan-model.html#localisation)
/// [tooltip] - tooltip text. See [Localisation](https://www.takkanblog.co.uk/user-guide/takkan-model.html#localisation)
/// [height] - is set here because both read and edit particles need to be the same height to avoid display 'jumping' when switching between read and edit modes.
/// [isStatic] - returns true if [staticData] is non-null
/// [listEntryConfig] - see [Content.listEntryConfig]
///
/// [fieldName] is set by the parent on a call to [Composer.init]
@JsonSerializable(explicitToJson: true)
class Part extends PresentationProperties
    implements PageChild, PanelChild, HasFinal<PartFinal>, LateNamed {
  Part({
    super.document,
    super.property,
    super.dataInstance,
    super.controlEdit,
  }
      // {
      // super.caption,
      // this.readOnly = false,
      // this.height,
      // required IField schema,
      // required this.traitName,
      // super.help,
      // super.controlEdit = ControlEdit.firstLevelPanels,
      // super.id,
      // super.listEntryConfig,

      // this.tooltip,
      // }
      ) : super();

  factory Part.fromJson(Map<String, dynamic> json) => _$PartFromJson(json);

  // @FieldJsonConverter()
  // final IField schema;
  // final bool readOnly;

  // final Help? help; // TODO: Also add to props
  // final String? tooltip;
  // final double? height;
  // final String traitName;

  // @JsonKey(includeToJson: false, includeFromJson: false)
  // @override
  // List<Object?> get props =>[];
  // [...super.props, readOnly, height, traitName, tooltip];

  @override
  Map<String, dynamic> toJson() => _$PartToJson(this);

  // @override
  // DebugNode get debugNode {
  //   final List<DebugNode> children = List.empty(growable: true);
  //   if (dataProviderIsDeclared) {
  //     final DebugNode? dn = dataProvider?.debugNode;
  //     if (dn != null) {
  //       children.add(dn);
  //     }
  //   }
  //   return DebugNode(this, children);
  // }

  // @override
  void doValidate(ValidationWalkerCollector collector) {
    // super.doValidate(collector);
    throw UnimplementedError('Part.doValidate');
    // if (property == null && staticData == null) {
    //   collector.messages.add(ValidationMessage(
    //       item: this,
    //       msg:
    //       "a Part must either provide a 'property' to bind to data, or define 'staticData' to be presented"));
    // }
  }

  @override
  PartFinal get asFinal => PartFinal(
        showEditControls: showEditControls,
        document: document,
        content: IList(),
        route: route,
        property: property,
        dataInstance: dataInstance,
      );

  @override
  String get routeSeparator => '.';

  /// [showEditControls] is set to show edit controls or not, depending on
  /// the values in [controlEdit]. [controlEdit] may have been inherited or set
  /// explicitly in this instance.  Cascading inherited values must be done
  /// before this is calculated
  @override
  bool convertControlEdit(PresentationProperties parent) {
    return controlEdit.contains(ControlEdit.allParts) ||
        controlEdit.contains(ControlEdit.yes);
  }

  @override
  void lateName(String name) => applyName(name);

  @override
  List<Object?> get subElements => [];

}

/// [PartFinal] is the final version of a [Part].
///
/// It is the only [Final] implementation which provides access to the
/// [document] and [dataInstance] that provide the data.
///
///
class PartFinal implements Final, PageChildFinal, PanelChildFinal {
  final String route;
  final IList<PageChildFinal> content;
  final String property;
  final Document document;
  final DatasourceInstance dataInstance;
  final bool showEditControls;

  const PartFinal({
    required this.property,
    required this.route,
    required this.content,
    required this.document,
    required this.dataInstance,
    required this.showEditControls,
  });
}
