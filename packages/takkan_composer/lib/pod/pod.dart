/// A [Composer] element which can contain other [Composer] elements, namely a
/// [Panel] or a [Page]
abstract class Pod{

}

/// A [Composer] element which can be contained by a [Pod]
abstract class PodContent{

}