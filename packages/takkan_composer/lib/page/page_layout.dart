import 'package:json_annotation/json_annotation.dart';
import 'package:takkan_common/common/constants.dart';
import 'package:takkan_common/common/log.dart';
import 'package:takkan_common/common/serial.dart';
import 'package:takkan_composer/page/page.dart';

part 'page_layout.g.dart';

abstract class PageLayout extends Serializable {}

@JsonSerializable(explicitToJson: true)
class DefaultPageLayout implements PageLayout {
  const DefaultPageLayout();

  factory DefaultPageLayout.fromJson(Map<String, dynamic> json) =>
      _$DefaultPageLayoutFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$DefaultPageLayoutToJson(this);
}

class PageLayoutJsonConverter
    extends JsonConverter<PageLayout, Map<String, dynamic>> {
  const PageLayoutJsonConverter();

  @override
  PageLayout fromJson(Map<String, dynamic> json) {
    final elementType = json[jsonClassKey];
    switch (elementType) {
      case 'DefaultPageLayout':
        return DefaultPageLayout.fromJson(json);

      default:
        final msg = 'PageLayout type $elementType not recognised';
        logType(runtimeType).e(msg);
        throw Exception(msg);
    }
  }

  @override
  Map<String, dynamic> toJson(PageLayout object) {
    final type = object.runtimeType;
    final outputMap = <String, dynamic>{};
    outputMap[jsonClassKey] = type.toString();
    outputMap.addAll(object.toJson());
    return outputMap;
  }
}

