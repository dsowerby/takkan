

import 'package:json_annotation/json_annotation.dart';
import 'package:takkan_common/common/constants.dart';
import 'package:takkan_common/common/inherited.dart';
import 'package:takkan_common/common/log.dart';
import 'package:takkan_common/common/serial.dart';

import '../panel/panel.dart';
import '../part/part.dart';
import 'page.dart';


/// Type safety for items which can be held as content within a [Page], namely [Page],
/// [Panel] or [Part].  See also [PageChildFinal]
abstract class PageChild extends Serializable{
}

/// Implementations which can be held as content within a [PageFinal], namely
/// [PageFinal], [PanelFinal] or [PartFinal].  See also [PageChild]
abstract class PageChildFinal extends Final{}


class PageChildJsonConverter
    extends JsonConverter<PageChild, Map<String, dynamic>> {
  const PageChildJsonConverter();

  @override
  PageChild fromJson(Map<String, dynamic> json) {
    final elementType = json[jsonClassKey];
    switch (elementType) {
      case 'Page':
        return Page.fromJson(json);
      case 'Panel': return Panel.fromJson(json);
      case 'Part': return Part.fromJson(json);
      default:
        final msg = 'PageLayout type $elementType not recognised';
        logType(runtimeType).e(msg);
        throw Exception(msg);
    }
  }

  @override
  Map<String, dynamic> toJson(PageChild object) {
    final type = object.runtimeType;
    final outputMap = <String, dynamic>{};
    outputMap[jsonClassKey] = type.toString();
    outputMap.addAll(object.toJson());
    return outputMap;
  }
}