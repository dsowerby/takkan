// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'page.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Page _$PageFromJson(Map<String, dynamic> json) => Page(
      layout: json['layout'] == null
          ? const DefaultPageLayout()
          : const PageLayoutJsonConverter()
              .fromJson(json['layout'] as Map<String, dynamic>),
      slug: json['slug'] as String?,
      content: (json['content'] as Map<String, dynamic>).map(
        (k, e) => MapEntry(k,
            const PageChildJsonConverter().fromJson(e as Map<String, dynamic>)),
      ),
    );

Map<String, dynamic> _$PageToJson(Page instance) => <String, dynamic>{
      'content': instance.content
          .map((k, e) => MapEntry(k, const PageChildJsonConverter().toJson(e))),
      'layout': const PageLayoutJsonConverter().toJson(instance.layout),
      'slug': instance.slug,
    };
