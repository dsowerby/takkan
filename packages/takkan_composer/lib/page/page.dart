import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:takkan_common/common/inherited.dart';
import 'package:takkan_common/common/walker.dart';
import 'package:takkan_composer/page/page_layout.dart';
import 'package:takkan_schema/schema/document/document.dart';

import '../content/control_edit.dart';
import '../feature/feature.dart';
import 'page_child.dart';
import '../common/properties.dart';

part 'page.g.dart';

/// Page is a conceptual page as viewed by end user.
///
/// A [Page] may contain instances of [Page], [Panel] or [Part], represented
/// by the [PageChild] interface in [content].  The key in [content] is the
/// property name of the underlying schema for the page.
///
/// At a presentational level, a [Page] does not actually contain another [Page],
/// but at schema level translates to a pointer to the child page.  The [route]
/// of the child page will also default to reflect its hierarchy with its parent.
///
/// [route] is hierarchical by default, unless explicitly overridden.
@JsonSerializable(explicitToJson: true)
class Page extends PresentationProperties
    implements PageChild, HasFinal<PageFinal>, LateNamed {
  @PageChildJsonConverter()
  final Map<String, PageChild> content;
  @PageLayoutJsonConverter()
  final PageLayout layout;

  /// This will be set from map key in parent via [lateName]
  String? slug;

  Page({
    this.layout = const DefaultPageLayout(),
    this.slug,
    required this.content,
    String? name,
    super.document,
    super.dataInstance,
    super.controlEdit,
  }) : super(property: slug);

  factory Page.fromJson(Map<String, dynamic> json) => _$PageFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$PageToJson(this);

  @override
  PageFinal get asFinal => PageFinal(
        showEditControls: showEditControls,
        property: property,
        route: route,
        content: IList(content.values
            .map((v) => (v as HasFinal).asFinal as PageChildFinal)),
      );

  /// [showEditControls] is set to show edit controls or not, depending on
  /// the values in [controlEdit]. [controlEdit] may have been inherited or set
  /// explicitly in this instance.  Cascading inherited values must be done
  /// before this is calculated
  @override
  bool convertControlEdit(PresentationProperties parent) {
    return controlEdit.contains(ControlEdit.allPages) ||
        controlEdit.contains(ControlEdit.yes) ||
        controlEdit.contains(ControlEdit.firstLevelPages) &&
            (parent is Feature);
  }

  @override
  void lateName(String name) {
    slug = name;
    applyName(name);
  }

  @override
  List<Object?> get subElements => [content];
}

class PageFinal implements Final, PageChildFinal {
  final String route;
  final String property;
  final IList<PageChildFinal> content;
  final bool showEditControls;

  const PageFinal({
    required this.route,
    required this.content,
    required this.property,
    required this.showEditControls,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PageFinal &&
          runtimeType == other.runtimeType &&
          route == other.route &&
          property == other.property &&
          content == other.content;

  @override
  int get hashCode => Object.hash(route, content, property);
}
