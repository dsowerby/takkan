import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:takkan_common/common/inherited.dart';

import '../page/page.dart';

class RouteMapFinal implements Final{
  final IMap<String, PageFinal> routes;

  const RouteMapFinal({
    required this.routes,
  });

  operator [](String key) => routes[key];
}