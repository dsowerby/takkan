import 'package:json_annotation/json_annotation.dart';

// part 'composer_set.g.dart';

/// A [ComposerSet] represents a set of [Schema] versions, and is primarily used
/// to support version control.  It is therefore not expected generally to be
/// used in a client application, unless the client needs to support multiple
/// versions
///
/// A [ComposerSet] is defined by a [baseVersion] with 0 or more [diffs] defining
/// subsequent versions.  All versions have a version index which must increment
/// by at least 1 for each new version.  See [Version] for more detail.
///
/// Each version has a [VersionStatus], which is used to inform both server
/// code and the client.  The status of a version can be changed by
/// [changeVersionStatus]. Takkan does not by default impose any rules on how
/// transitions from one status to another can be made.  You can change this by
/// passing your own implementation of [VersionStatusChangeRules] to specify your
/// own lifecycle
///
/// During [init] the [baseVersion] is initialised, and versions created (expanded)
/// for all [diffs] except those that have [VersionStatus.excluded].
///
/// [activeDiffs] returns only diffs which are not [VersionStatus.excluded]
///
/// [changeBaseVersion] changes the base version.
/// TODO: need a JsonConverter for VersionStatusChangeRules so we can use the interface here
// @JsonSerializable(explicitToJson: true)
// class ComposerSet {
//   ComposerSet({
//     required this.baseVersion,
//     List<SchemaDiff>? diffs,
//     required this.schemaName,
//     DefaultVersionStatusChangeRules? statusRules,
//   })  : _diffs = diffs ?? [],
//         statusRules = statusRules ?? const DefaultVersionStatusChangeRules();
//
//   factory ComposerSet.fromJson(Map<String, dynamic> json) =>
//       _$SchemaSetFromJson(json);
//   Schema baseVersion;
//   final List<SchemaDiff> _diffs;
//   final String schemaName;
//   final Map<int, Schema> _schemaVersions = <int, Schema>{};
//   final DefaultVersionStatusChangeRules statusRules;
//
//   /// Expands and creates all versions from [baseVersion] plus [diffs]
//   /// The [baseVersion] is also the first version
//   /// Initialises the [baseVersion]
//   void init() {
//     baseVersion.doInit(schemaName: schemaName);
//     _expandVersions();
//   }
//
//   /// Return only those [Schema] instances where its status is
//   /// [VersionStatusExtension.active]
//   Set<Schema> get activeSchemas => _schemaVersions.values
//       .where((schema) => schema.status.isActive)
//       .toSet();
//
//   /// Return only those [SchemaDiff] instances where its status is
//   /// [VersionStatusExtension.active]
//   Set<SchemaDiff> get activeDiffs =>
//       _diffs.where((element) => element.status.isActive).toSet();
//
//   /// If you want only those versions which have not been removed, use
//   /// [validSchemaVersions]
//   Map<int, Schema> get schemaVersions => Map.from(_schemaVersions);
//
//   /// Returns only those versions which are not [VersionStatus.excluded]
//   /// If you want all versions, use [schemaVersions]
//   ///
//   /// This relies on the versions being built from [validDiffs], using
//   /// [_expandVersions], and access must therefore be preceded by a call to [init]
//   Map<int, Schema> get validSchemaVersions {
//     final result = Map<int, Schema>.from(_schemaVersions);
//     result.removeWhere((key, value) => value.status == VersionStatus.excluded);
//     return result;
//   }
//
//   /// Returns all diffs regardless of [VersionStatus]
//   List<SchemaDiff> get diffs => List.from(_diffs);
//
//   /// Returns all diffs except those that are [VersionStatus.excluded]
//   List<SchemaDiff> get validDiffs =>
//       _diffs.where((element) => element.status.isValid).toList();
//
//   /// Expands the diffs and populates [_schemaVersions].  [validDiffs] are applied in
//   /// turn to the [baseVersion], producing a new, full [Schema] instance at
//   /// each version.  These instances are available at [schemaVersions]
//   void _expandVersions() {
//     // diffs.sort((a, b) => a.versionIndex.compareTo(b.versionIndex));
//     // Schema latestSoFar = baseVersion;
//     // _schemaVersions[baseVersion.versionIndex] = baseVersion;
//     //
//     // // Step through valid diffs and apply to base version of schema, thus producing
//     // // a new version.  New version is added to _versions.
//     // for (final SchemaDiff diff in validDiffs) {
//     //   final nextVersionOfSchema = diff.applyTo(latestSoFar);
//     //   nextVersionOfSchema.init(schemaName: schemaName);
//     //   _schemaVersions[nextVersionOfSchema.versionIndex] = nextVersionOfSchema;
//     //   latestSoFar = nextVersionOfSchema;
//     // }
//
//     // TODO: expanding versions will need to be done at the ComposerSet level
//     throw UnimplementedError('TODO: implement _expandVersions');
//   }
//
//   Map<String, dynamic> toJson() => _$SchemaSetToJson(this);
//
//   /// This relies on the versions being built from [validDiffs], using
//   /// [_expandVersions], and must therefore/ be preceded by a call to [init]
//   Schema schemaVersion(int versionIndex) {
//     final v = _schemaVersions[versionIndex];
//     if (v == null) {
//       throw TakkanException('Schema version $versionIndex not found');
//     }
//     return v;
//   }
//
//   SchemaDiff diffVersion(int versionIndex) {
//     final diff =
//     diffs.firstWhere((element) => element.versionIndex == versionIndex);
//     if (diff == null) {
//       throw TakkanException('SchemaDiff version $versionIndex not found');
//     }
//     return diff;
//   }
//
//   /// This relies on the versions being built from [diffs], using [_expandVersions],
//   /// and must therefore/ be preceded by a call to [init]
//   bool containsSchemaVersion(int versionIndex) {
//     return _schemaVersions.containsKey(versionIndex);
//   }
//
//   bool containsDiffVersion(int versionIndex) {
//     return diffs.any((element) => element.versionIndex == versionIndex);
//   }
//
//   /// Make a change to the status of the version identified by its [versionIndex].
//   /// A change is only made if the [statusRules] are met.
//   ///
//   /// The change is applied to [schemaVersions] and [diffs], except when the
//   /// change is applied to the [baseVersion] as there will be no diff for that.
//   void changeVersionStatus(int versionIndex, VersionStatus newStatus) {
//     // This will throw an exception if versionIndex not valid
//     final versionToChange = schemaVersion(versionIndex);
//     final currentStatus = versionToChange.status;
//
//     if (currentStatus == newStatus) {
//       logType(runtimeType).i(
//           'Version at index $versionIndex is already at $newStatus, no change made');
//       return;
//     }
//
//     final isBaseVersion = versionIndex == baseVersion.versionIndex;
//     // Check the rules.  Rules may also throw an exception
//     final ruleCheck = statusRules.checkRules(
//         currentStatus: currentStatus,
//         newStatus: newStatus,
//         isBaseVersion: isBaseVersion);
//
//     // Do nothing if we failed to pass the rules
//     if (!ruleCheck) {
//       return;
//     }
//     // if we are moved to released state and we want to maintain a unique release,
//     // we need to deprecate currently released version, if there is one. Do
//     // before changing to newStatus, so we don't forget which is which
//     if (newStatus == VersionStatus.released) {
//       if (statusRules.maintainUniqueReleasedVersion) {
//         // We will check for more than one just in case the rule has changed
//         final currentlyReleased = schemaVersions.values
//             .where((element) => element.status == VersionStatus.released);
//         for (final v in currentlyReleased) {
//           changeVersionStatus(v.versionIndex, VersionStatus.deprecated);
//         }
//       }
//     }
//     // apply the version change
//     _doChangeVersionStatus(versionToChange, newStatus);
//   }
//
//   void _doChangeVersionStatus(Schema versionToChange, VersionStatus newStatus) {
//     // create a new version instance with new status
//     final newVersionInstance = versionToChange.version.withStatus(newStatus);
//
//     // change status in Schema
//     versionToChange.version = newVersionInstance;
//     final int versionIndex = versionToChange.versionIndex;
//     // change diff as well, if there is one (only time there would not be is if
//     // we are changing the base version)
//     if (versionIndex != baseVersion.versionIndex) {
//       final diff =
//       diffs.firstWhere((element) => element.versionIndex == versionIndex);
//       if (diff == null) {
//         // This should not happen - diffs should not have been be removed unless
//         // expanded schema is removed as well
//         throw TakkanException('There is no SchemaDiff for $versionIndex');
//       }
//       diff.version = newVersionInstance;
//     }
//   }
//
//   /// throws TakkanException if the version requested is the base version;
//   void _checkNotBaseVersion(int versionIndex, String action, String reason) {
//     if (versionIndex == baseVersion.versionIndex) {
//       throw TakkanException('Cannot $action base version $reason');
//     }
//   }
//
//   /// Adds a new version by specifying it as a[SchemaDiff].  Its status is
//   /// determined by the [SchemaDiff.version].  If the [Version.versionIndex]] already
//   /// exists, a TakkanException is thrown.
//   ///
//   /// The versionIndex of [diff] must be greater than any existing versionIndex,
//   /// but does not need to be contiguous.
//   ///
//   /// The newly added entry in [diffs] is expanded into [_schemaVersions]
//   void addVersion(SchemaDiff diff) {
//     // Reject if the versionIndex of diff is not greater than those already in diffs
//     if (diffs.any((element) => element.versionIndex >= diff.versionIndex)) {
//       throw const TakkanException(
//           'An added version must have a versionIndex greater than any existing '
//               'versionIndex');
//     }
//     _diffs.add(diff);
//     _schemaVersions.clear();
//     _expandVersions();
//   }
//
//   /// Changes the current base version to the specified [versionIndex]. All
//   /// versions prior to this have their status set to [VersionStatus.expired]
//   /// unless they have been previously [VersionStatus.excluded].
//   void changeBaseVersion(int versionIndex) {
//     _checkNotBaseVersion(
//         versionIndex, 'change base ', ' as base already at $versionIndex');
//     final oldBaseIndex = baseVersion.versionIndex;
//     final newBase = schemaVersion(versionIndex);
//     // expire all versions prior to this new base version
//     for (int i = oldBaseIndex; i < versionIndex; i++) {
//       if (containsSchemaVersion(i)) {
//         if (schemaVersion(i).status != VersionStatus.excluded) {
//           changeVersionStatus(i, VersionStatus.expired);
//         }
//       }
//     }
//     baseVersion = newBase;
//   }
// }