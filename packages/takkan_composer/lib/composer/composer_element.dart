// ignore_for_file: must_be_immutable
/// See comments on [ComposerElement]
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:takkan_common/common/inherited.dart';
import 'package:takkan_common/common/serial.dart';
import 'package:takkan_common/common/walker.dart';

import 'message.dart';




/// A single [Composer] instance is the root of a tree structure.
///
/// This [ComposerElement] is the base class for the major components of that
/// tree, but not all elements, as there are references to classes outside the
/// [Composer] structure.
///
/// It contains these properties:
///
///  - The [parent] of this [ComposerElement], which is translated by the Takkan
///  client to become a parent Widget.
///
/// There are three types of id, used to support testing and debugging.
///
/// - The [uid] is a generated unique id within the scope of a [parent].  It
/// is also used as part of the [debugId]
///
/// - [id] is optional. If set it overrides the generated [uid] and becomes
/// part of the [debugId]. It is useful to identify a specific Widget during
/// testing, or where ambiguity arises from the use of [uid]. Ambiguity is
/// possible, although unlikely unless there are two components with the same
/// caption or name.
///
/// - The [debugId] is a hierarchical 'path' id unique within the [Composer]
/// instance. The hierarchical nature of this key means a specific [ComposerElement]
/// instance can be identified via its parent chain.  This [debugId] also
/// becomes the Widget key of the item's associated Widget, especially useful for
/// functional testing.
///
/// - [childIndex] is the child index within a parent where it is held in a
/// list. Defaults to -1.  Its primary purpose is to differentiate between
/// multiple instances of the same type in a list.
///
/// Testing of equality uses Equatable, made possible because this is an
/// immutable class.
abstract class ComposerElement extends Equatable
    with Walkable
    implements Serializable {
  const ComposerElement({
    String? id,
    String? uid,
    String? debugId,
    this.childIndex=-1,
  }) : _id=id, _uid=uid, _debugId=debugId;

  final String? _id;
  final String? _uid;

  /// Used for Widget and Functional testing.  This also becomes the Widget key
  /// in [Page], [Part] and [Panel] instances
  final String? _debugId;
  final int childIndex;

  @override
  Map<String, dynamic> toJson();

  String get id=>checkInherited(this,_id);
  String get debugId=>checkInherited(this,_debugId);
  String get uid=>checkInherited(this,_uid);



  @JsonKey(includeToJson: false, includeFromJson: false)
  @override
  List<Object?> get props => [id, uid, debugId, childIndex];

  void doValidate(ValidationWalkerCollector collector) {}

// TODO: What was this about?  HOw did it work?
// DebugNode get debugNode => DebugNode(this, const []);
}
class InitWalkerParams extends WalkerParams {
  const InitWalkerParams({this.name, this.index, this.useCaptionsAsIds = true});

  final int? index;
  final String? name;
  final bool useCaptionsAsIds;
}

class ValidationWalkerCollector extends WalkerParams {
  final List<ValidationMessage> messages = [];
}
