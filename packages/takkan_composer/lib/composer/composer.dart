import 'dart:collection';

import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:takkan_common/common/inherited.dart';
import 'package:takkan_common/common/version.dart';
import 'package:takkan_common/common/walker.dart';
import "package:meta/meta.dart";
import 'package:takkan_schema/schema/app/app_schema.dart';
import 'package:takkan_schema/schema/document/document.dart';
import 'package:takkan_schema/schema/document/document_library.dart';

import '../content/control_edit.dart';
import '../feature/feature.dart';
import '../page/page.dart';
import '../common/properties.dart';
import 'route_map.dart';

/// - [features] group functionality together.
/// - [version] becomes the application version
class Composer extends PresentationProperties with Walkable {
  final Version version;
  final Map<String, Feature> features;

  Composer({
    required this.features,
    required this.version,
    required String name,
    super.document,
    super.dataInstance,
    super.controlEdit = const [ControlEdit.allPages],
  }) : super(property: name, route: '');

  /// Kicks off a chain of initialization down through the tree, returning a const
  /// structure.  All of the elements in the returned structure are const, and are
  /// held in classes suffixed with Final.  even the collections contained in the
  /// structure are immutable.
  ///
  /// Different sub-element types will update in different ways, but many parents
  /// use a map key to define a property of the child values held in the map.
  ///
  /// This allows the developer to define the structure more fluently, while
  /// retaining consistency.
  ///
  ///
  /// **NOTE** Use the return value or else it will appear that nothing has
  /// happened.  This is to allow the whole structure to be 'const'
  ComposerFinal init() {
    final initVisitor = InitVisitor();
    walk(
      EmptyWalkable(),
      [initVisitor],
    );

    final updatedFeatures = features.map(
        (key, value) => MapEntry<String, FeatureFinal>(key, value.asFinal));
    final IMap<String, PageFinal> routes = _assembleRoutes(updatedFeatures);



    return ComposerFinal(
        schema: initVisitor.library.toAppSchema(appVersion: version),
        routeMap: RouteMapFinal(routes: routes),
        version: version,
        features: IMap(updatedFeatures));
  }

  IMap<String, PageFinal> _assembleRoutes(
      Map<String, FeatureFinal> updatedFeatures) {
    final Queue<PageFinal> q = Queue();
    for (final feature in updatedFeatures.values) {
      q.addAll(feature.pages);
    }
    final pageCollection = <String, PageFinal>{};
    while (q.isNotEmpty) {
      final page = q.removeFirst();
      pageCollection[page.route] = page;
      q.addAll(page.content.whereType<PageFinal>());
    }
    return pageCollection.lock;
  }

  @override
  List<Object?> get subElements => [features, version];

  /// [Composer] does not actually use this for itself
  @override
  bool convertControlEdit(PresentationProperties parent) {
    return false;
  }
}

@immutable
class ComposerFinal implements Final {
  final Version version;
  final IMap<String, FeatureFinal> features;
  final RouteMapFinal routeMap;
  final AppSchema schema;

  // TODO: Schema needs to be mapped to Datastore
  const ComposerFinal({
    required this.features,
    required this.version,
    required this.routeMap,
    required this.schema,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ComposerFinal &&
          runtimeType == other.runtimeType &&
          version == other.version &&
          features == other.features &&
          routeMap == other.routeMap;

  @override
  int get hashCode => Object.hash(version, features, routeMap);
}

class DocumentCollationVisitor implements WalkVisitor<EmptyWalkerParams> {
  final List<Document> collected = [];

  @override
  void visit(Walkable parent, Object entry,
      {WalkerParams params = const EmptyWalkerParams()}) {
    if (entry is Document) {
      collected.add(entry);
    }
  }
}

class InitVisitor implements WalkVisitor<EmptyWalkerParams> {
  final library = DocumentLibrary();

  /// In order to merge properties both [parent] and [entry] need to be
  /// [PresentationProperties]
  @override
  void visit(Walkable parent, Object entry,
      {WalkerParams params = const EmptyWalkerParams()}) {
    if (entry is PresentationProperties && parent is PresentationProperties) {
      entry.applyCommonValues(parent, library);
    }
  }
}


