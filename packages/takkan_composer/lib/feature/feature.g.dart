// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feature.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Feature _$FeatureFromJson(Map<String, dynamic> json) => Feature(
      pages: (json['pages'] as Map<String, dynamic>).map(
        (k, e) => MapEntry(k, Page.fromJson(e as Map<String, dynamic>)),
      ),
      name: json['name'] as String,
      slug: json['slug'] as String?,
    );

Map<String, dynamic> _$FeatureToJson(Feature instance) => <String, dynamic>{
      'pages': instance.pages.map((k, e) => MapEntry(k, e.toJson())),
      'slug': instance.slug,
      'name': instance.name,
    };
