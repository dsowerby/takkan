import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:takkan_common/common/inherited.dart';
import 'package:takkan_common/common/walker.dart';
import 'package:takkan_schema/schema/document/document.dart';

import '../composer/composer.dart';
import '../page/page.dart';
import '../common/properties.dart';

part 'feature.g.dart';

@JsonSerializable(explicitToJson: true)
class Feature extends PresentationProperties
    with Walkable
    implements HasFinal<FeatureFinal> , LateNamed{
  final Map<String, Page> pages;
  String? slug;

  Feature({
    required this.pages,
    required String name,
    super.document,
    super.dataInstance,
    super.controlEdit,
    this.slug,
  }) : super(property: name);

  factory Feature.fromJson(Map<String, dynamic> json) =>
      _$FeatureFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$FeatureToJson(this);

  @override
  List<Object?> get subElements => [pages];

  String get name => property;

  @override
  FeatureFinal get asFinal => FeatureFinal(
      name: name,
      slug: property,
      route: '/$property',
      pages: IList(
        pages.values.map((e) => e.asFinal),
      ));

  /// [Feature] does not actually use this for itself
  @override
  bool convertControlEdit(PresentationProperties parent) {
    return false;
  }

  @override
  void lateName(String name) => applyName(name);
}

class FeatureFinal implements Final {
  final String name;
  final IList<PageFinal> pages;
  final String slug;
  final String route;

  const FeatureFinal({
    required this.name,
    required this.slug,
    required this.pages,
    required this.route,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FeatureFinal &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          slug == other.slug &&
          pages == other.pages;

  @override
  int get hashCode => Object.hash(name, pages, slug);
}
