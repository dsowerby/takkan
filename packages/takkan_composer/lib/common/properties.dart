import 'package:json_annotation/json_annotation.dart';
import 'package:takkan_common/common/data.dart';
import 'package:takkan_common/common/inherited.dart';
import 'package:takkan_common/common/walker.dart';
import 'package:takkan_schema/schema/document/document.dart';
import 'package:takkan_schema/schema/document/document_library.dart';

import '../content/control_edit.dart';

/// A base class of properties used by the 'presentation' elements of a [Composer],
/// used also in supporting of inheritance from [Composer] down through
/// [Feature], [Page], [Panel] and [Part].  May be used elsewhere.
///
/// [_property] must be unique within the context of a parent, and if set
/// directly,  must conform to any case rules that apply.  It may be
/// declared differently in sub-classes, to make more sense in context.  For
/// example, a [Page] declares this property as [Page.slug] as that makes sense
/// for a page, but the value of slug is actually stored in [_property]
///
/// [_route] is always unique within the context of a [Composer].  It is
/// hierarchical, and extends the usual route structure to include properties,
/// for example:
///
/// ```txt
/// myapp/core/contact_details/address.street
/// ```
///
/// [route] is always calculated as a combination of the parent route with
/// [property] appended.  Note the use of [routeSeparator].
///
/// In all cases a value set directly in a child overrides the same value
/// from its parent.  A bit like real life!
abstract class PresentationProperties with Walkable {
  String? _property;
  String? _route;
  Document? _document;
  DatasourceInstance? _dataInstance;
  bool? _showEditControls;

  String get property => checkInherited(this, _property);

  String get route => checkInherited(this, _route);

  bool get showEditControls => checkInherited(this, _showEditControls);

  @JsonKey(includeFromJson: false, includeToJson: false)
  List<ControlEdit> get controlEdit => _controlEdit;

  @JsonKey(includeFromJson: false, includeToJson: false)
  List<ControlEdit> _controlEdit;

  @JsonKey(includeFromJson: false, includeToJson: false)
  Document get document => checkInherited(this, _document);

  @JsonKey(includeFromJson: false, includeToJson: false)
  DatasourceInstance get dataInstance => checkInherited(this, _dataInstance);

  PresentationProperties({
    String? property,
    String? route,
    Document? document,
    DatasourceInstance? dataInstance,
    List<ControlEdit> controlEdit = const [],
  })  : _controlEdit = controlEdit,
        _document = document,
        _property = property,
        _dataInstance = dataInstance,
        _route = route;

  void applyCommonValues(
      PresentationProperties inherited, DocumentLibrary library) {
    _route = _route = '${inherited.route}$routeSeparator$property';
    _dataInstance = _dataInstance ?? inherited._dataInstance;
    _controlEdit =
        (_controlEdit.isEmpty) ? inherited._controlEdit : _controlEdit;
    _showEditControls = convertControlEdit(inherited);
    _document = _document ?? inherited._document;


    if (_document != null) {
      library.addDocument(document,dataInstance);
    }
  }

  void applyName(String name) {
    _property = name;
  }

  /// [showEditControls] is set to show edit controls or not, depending on
  /// the values in [controlEdit]. [controlEdit] may have been inherited or set
  /// explicitly in this instance.  Cascading inherited values must be done
  /// before this is calculated
  bool convertControlEdit(PresentationProperties parent);

  String get routeSeparator => '/';
}
