// import 'package:takkan_schema/schema/document/document.dart';
// import 'package:takkan_schema/schema/field/field.dart';
//
// import 'page/page.dart';
// import 'panel/panel.dart';
// import 'part/part.dart';
//
// /// Something like a 5 star review widget
// /// Try using in multiple places
// class ReviewPart extends Part {
//   ReviewPart({required super.schema, super.traitName='default', required super.schemaElement});
// }
//
// /// A panel to add / edit an address. This would be a Class in Back4App.
// /// Try using
// ///  - in multiple places
// ///  - also in 2 places, but sharing table?
// class AddressPanel extends Panel {
//   AddressPanel()
//       : super(content: [
//           Part(schemaElement: Document(fields: {}),
//               schema: Field<String>(),
//               traitName: 'default', )
//         ]);
// }
//
// /// A special page that does its own thing
// class SpecialPage extends Page {
//   SpecialPage({super.layout, super.content = const []});
// }
//
// class Text extends Part {
//
//   const Text({
//     super.tooltip,
//     required super.schema,
//     required super.traitName, required super.schemaElement,
//   });
// }
